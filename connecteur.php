<?php
/**
* Connecteur.php - Recupère, envoie et transfère les informations à la base de données
*/

/**
* Ouvre une connexion à la base de donnée locale.
* @return \PDO
*/
function ConnectLocale()
{
        try
        {
                $bdd = new PDO('mysql: host=localhost;port=;dbname=bd_ppe_agenda;charset=utf8', 'root', '', array(
                                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                ));
                //Le dernier paramètre permet d'avoir des messages d'erreur beaucoup plus clairs lorsque sur requête SQL
        }
        catch (PDOException $e)
        {
                die("Erreur : " . $e->getMessage());
        }
        return $bdd;
}

/**
* Ouvre une connexion à la base de donnée du contexte.
* @return \PDO
*/
function ConnectContexte()
{
        try
        {
                $bdd = new PDO('mysql:host=192.168.108.20;port=;dbname=vn_formation;charset=utf8', 'vn_formation', 'azerty', array(
                                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                ));
                //Le dernier paramètre permet d'avoir des messages d'erreur beaucoup plus clairs lorsque sur requête SQL
        }
        catch (PDOException $e)
        {
                die("Erreur : " . $e->getMessage());
        }
        return $bdd;
}

/**
* Ouvre une connexion à la base de donnée du serveur OVH.
* @return \PDO
*/
function Connect()
{
        try
        {
                $bdd = new PDO('', array(
                                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                ));
                //Le dernier paramètre permet d'avoir des messages d'erreur beaucoup plus clairs lorsque sur requête SQL
        }
        catch (PDOException $e)
        {
                die("Erreur : " . $e->getMessage());
        }
        return $bdd;
}

/**
* Selectionne dans la base l'utilisateur correspondant au login et au mot de passe saisi
* @param string $login
* @param string $mdp
* @return array
*/
function SelectAll($login, $mdp)
{
        //Connection à la base
        $bdd = Connect();
        $sql = "SELECT uti_id,uti_vue,uti_nom,uti_pre,uti_admin FROM utilisateur WHERE uti_log=:login AND uti_mdp= md5(:mdp)";
        
        $reponse = $bdd->prepare($sql);
        $reponse->bindParam(':login', $login);
        $reponse->bindParam(':mdp', $mdp);
        $reponse->execute();
        $resultat = $reponse->fetchAll();

        return $resultat;
}

/**
* Selectionne dans la base tous les utilisateurs pour pouvoir afficher leur agenda
* @return array
*/
function SelectCollegues()
{
        //Connection à la base
        $bdd = Connect();
        $sql = "SELECT uti_id,uti_nom,uti_pre,uti_log FROM utilisateur";

        $reponse = $bdd->prepare($sql);
        $reponse->execute();
        $resultat = $reponse->fetchAll();

        return $resultat;
}

/**
* Modification de la vue préférée au moment de la déconnexion
*/
function VuePreferee()
{
        $bdd     = Connect();
        $sql     = "UPDATE utilisateur SET uti_vue=:vue WHERE uti_id=:id";
        $reponse = $bdd->prepare($sql);
        $reponse->bindParam(':vue', $_SESSION['vue']);
        $reponse->bindParam(':id', $_SESSION['id']);
        $reponse->execute();
}

/**
* Select qui retourne tous les événements du mois affiché
* @param int $id
* @param string $dateDebut
* @param string $dateFin
* @return array
*/
function SelectEvents($id, $dateDebut, $dateFin)
{
        //Connection à la base
        $bdd = Connect();
        $sql = "SELECT * FROM evenement WHERE eve_uti=:id AND eve_debut BETWEEN :dateDebut and :dateFin ORDER BY eve_debut";

        $reponse = $bdd->prepare($sql);
        $reponse->bindParam(':id', $id);
        $reponse->bindParam(':dateDebut', $dateDebut);
        $reponse->bindParam(':dateFin', $dateFin);
        $reponse->execute();
        $resultat = $reponse->fetchAll();

        return $resultat;
}

/**
 * Retourne tous les évenements d'un utilisateur
 * @param int $id
 * @return array
 */
function SelectEveryEvent($id)
{
        //Connexion à la base
        $bdd      = Connect();
        $sql      = "SELECT * FROM evenement WHERE eve_uti=:id";
        $reponse  = $bdd->prepare($sql);
        $reponse->bindParam(':id', $id);
        $reponse->execute();
        $resultat = $reponse->fetchAll();
        return $resultat;
}

/**
 * Mise à jour d'un événement
 * @param string $intitule
 * @param string $description
 * @param string $dateDebut
 * @param string $dateFin
 * @param int $etatDispo
 * @param int $id
 * @return boolean
 */
function UpdateEvent($intitule, $description, $dateDebut, $dateFin, $etatDispo, $id)
{
        $bdd         = Connect();
        $intitule    = $bdd->quote($intitule);
        $description = $bdd->quote($description);
        $sql         = "UPDATE evenement SET eve_lib=$intitule, eve_desc=$description, eve_debut='$dateDebut', eve_fin='$dateFin' WHERE eve_id=$id";
        try
        {
                $reponse     = $bdd->prepare($sql);
                $update      = $reponse->execute();
                return $update;
        }
        catch (PDOException $e)
        {
                return false;
        }
}

/**
 * Suppression d'un événement
 * @param int $id
 * @return boolean
 */
function DeleteEvent($id)
{
        $bdd = Connect();

        $sql = "DELETE FROM evenement WHERE eve_id = :id";
        try
        {
                $stmt   = $bdd->prepare($sql);
                $stmt->bindParam(':id', $id);
                $delete = $stmt->execute();
                return $delete;
        }
        catch (Exception $ex)
        {
                return false;
        }
}


/**
 * Execute la requete enregistrée dans le fichier de sauvegarde.
 * Gère aussi le décryptage pour empêcher toute injection SQL via
 * cette méthode
 * @param string $requete
 * @return boolean
 */
function ExecuteRequete($requete)
{
        //Connexion à la base
        $bdd     = Connect();
        $insert  = false; //initialisé à false en cas d'exception
        $requete = Aes128_cbc_decrypt($requete); //décrypte le fichier
        $requete = str_replace(':id', $_SESSION['id'], $requete); //emplace les :id par l'id de la personne connectée
        $requete = explode(";", $requete); //scinde la requête en plusieurs au niveau des points-virgule (équivalent de split() en C#)
        for ($i = 0; $i < (sizeof($requete) - 1); $i++) //execute chaque requete. La dernière case de $requete contient juste le caractère de fin de fichier. 
        {
                $sql = $requete[$i] . ";";
                try
                {
                        $insert = $bdd->exec($sql);
                }
                catch (PDOException $e)
                {
                        break;
                }
        }
        return $insert;
}

/**
 * Génère un dump de la base de données
 * @return string
 */
function GenereDump()
{

        $bdd        = Connect();
        $maintenant = Maintenant();
        $nomfichier = dump_MySQL("127.0.0.1", "root", "", "bd_ppe_agenda", 2);
        return $nomfichier;
}

/**
 * Ecrit le fichier dump
 * @param string $serveur
 * @param string $login
 * @param string $password
 * @param string $base
 * @param string $mode
 * @return string
 */
function dump_MySQL($serveur, $login, $password, $base, $mode)
{
        error_reporting(E_ALL ^ E_DEPRECATED);
        $connexion = mysql_connect($serveur, $login, $password);
        mysql_select_db($base, $connexion);

        $entete     = "-- ----------------------\n";
        $entete    .= "-- dump de la base " . $base . " au " . date("d-M-Y") . "\n";
        $entete    .= "-- ----------------------\n\n\n";
        $creations  = "";
        $insertions = "\n\n";

        $listeTables = mysql_query("show tables", $connexion);
        while ($table = mysql_fetch_array($listeTables))
                {
                        // structure ou la totalité de la BDD
                        if ($mode == 1 || $mode == 2)
                                {
                                        $creations .= "-- -----------------------------\n";
                                        $creations .= "-- Structure de la table " . $table[0] . "\n";
                                        $creations .= "-- -----------------------------\n";
                                        $listeCreationsTables = mysql_query("show create table " . $table[0], $connexion);
                                        while ($creationTable = mysql_fetch_array($listeCreationsTables))
                                                {
                                                        $creations .= $creationTable[1] . ";\n\n";
                                                }
                                }
                        // données ou la totalité
                        if ($mode > 1)
                                {
                                        $donnees = mysql_query("SELECT * FROM " . $table[0]);
                                        $insertions .= "-- -----------------------------\n";
                                        $insertions .= "-- Contenu de la table " . $table[0] . "\n";
                                        $insertions .= "-- -----------------------------\n";
                                        while ($nuplet = mysql_fetch_array($donnees))
                                                {
                                                        $insertions .= "INSERT INTO " . $table[0] . " VALUES(";
                                                        for ($i = 0; $i < mysql_num_fields($donnees); $i++)
                                                                {
                                                                        if ($i != 0)
                                                                                        $insertions .= ", ";
                                                                        if (mysql_field_type($donnees, $i) == "string" || mysql_field_type($donnees, $i) == "blob")
                                                                                        $insertions .= "'";
                                                                        $insertions .= addslashes($nuplet[$i]);
                                                                        if (mysql_field_type($donnees, $i) == "string" || mysql_field_type($donnees, $i) == "blob")
                                                                                        $insertions .= "'";
                                                                }
                                                        $insertions .= ");\n";
                                                }
                                        $insertions .= "\n";
                                }
                }

        mysql_close($connexion);

        $fichierDump = fopen("sauvegarde.sql", "wb");
        fwrite($fichierDump, $entete);
        fwrite($fichierDump, $creations);
        fwrite($fichierDump, $insertions);
        fclose($fichierDump);

        return "sauvegarde.sql";
}
/**
* Insère l'événement crée par l'utilisateur dans la base
* @param string $intitule
* @param string $description
* @param string $dateDebut
* @param string $dateFin
* @param string $etatDispo
* @param string $id
* @return string
*/
function InsertEvent($intitule, $description, $dateDebut, $dateFin, $etatDispo, $id)
{
        $bdd         = Connect();
        $intitule    = $bdd->quote($intitule);
        $description = $bdd->quote($description);
        $sql         = "INSERT INTO evenement (eve_lib, eve_desc, eve_debut, eve_fin, eve_eta, eve_uti) VALUES
        ($intitule, $description, '$dateDebut', '$dateFin', $etatDispo, $id)";
        try
        {

                $insert = $bdd->exec($sql);
                return $insert;
        }
        catch (PDOException $e)
        {
                return $sql;
        }
}

/**
 * Remplace l'ancien mot de passe d'un utilisateur par un nouveau
 * @param int $idReinit
 * @param string $nouveauMdp
 * @return boolean
 */
function ReinitialisationMdp($idReinit, $nouveauMdp)
{
        $bdd = Connect();
        $sql = "UPDATE utilisateur SET uti_mdp = md5( :nouveauMdp ) WHERE uti_id = :idReinit";
        try
        {
                $nouveauMdp = $bdd->quote($nouveauMdp);
                $reponse = $bdd->prepare($sql);
                $reponse->bindParam(':nouveauMdp', $nouveauMdp);
                $reponse->bindParam(':idReinit', $idReinit);
                $update = $reponse->execute();
                return $update;
        }
        catch (PDOException $e)
        {
                return false;
        }
}

/**
* Select qui retourne tous les événements de l'utilisateur en fonction de leur intitulé
* @param int $id
* @param string $intitule
* @return array
*/
function SearchEvents($id, $intitule)
{
        //Connection à la base
        $bdd      = Connect();
        $intitule = $bdd->quote($intitule);
        $sql      = "SELECT eve_id, eve_lib, eve_desc, eve_debut, eve_fin, eve_eta, eve_uti,uti_nom, uti_pre FROM evenement join utilisateur on uti_id = eve_uti  WHERE eve_uti=:id AND eve_lib LIKE $intitule ORDER BY eve_debut";
        $reponse  = $bdd->prepare($sql);
        $reponse->bindParam(':id', $id);
        $reponse->execute();
        $resultat = $reponse->fetchAll();

        return $resultat;
}

/**
 * Séléctionne un événement à partir de son id
 * @param int $id
 * @return type
 */
function SelectEvent($id)
{
        //Connection à la base
        $bdd     = Connect();
        $sql     = "SELECT eve_id, eve_lib, eve_desc, eve_debut, eve_fin, eve_eta, eve_uti,uti_id,uti_nom, uti_pre FROM evenement join utilisateur on uti_id = eve_uti  WHERE eve_id=:id";
        $reponse = $bdd->prepare($sql);
        $reponse->bindParam(':id', $id);
        $reponse->execute();
        $resultat = $reponse->fetchAll();
        return $resultat;
}

/**
* Retourne la liste de tous les utilisateurs et les id correspondants
* @return array - tableau associatif
*/
function ListeAgendas()
{
        //Connection à la base
        $bdd     = Connect();
        $sql     = "SELECT uti_id, uti_nom, uti_pre FROM utilisateur ORDER BY uti_id";
        $reponse = $bdd->prepare($sql);
        $reponse->execute();
        $resultat = $reponse->fetchAll();
        return $resultat;
}
?>