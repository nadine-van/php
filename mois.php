<?php
/**
 * mois.php - Affiche le planning sur un mois
 */

session_start();
if (!isset($_SESSION['login']) || !isset($_SESSION['id']))
{
        header('Location: index.php?erreurDroits=1');
        exit();
}
$_SESSION['vue'] = 3;//3 correspond à la vue mois
?>

    <!DOCTYPE html>
    <html>
    <head>
	<title>Agenda M2L - Vue mensuelle</title>
        <link href="css/calendrier.css" rel='stylesheet' type='text/css' />
        <?php
        /* Par défaut à l'affichage le mois actuel est affiché. Si un autre
         * mois est communiqué en paramètre c'est celui-ci qui s'affiche. */
        if (isset($_GET["moiscourant"]) && isset($_GET["anneecourante"]))
        {
                /* si des paramètres ont été reçus, on vérifie que ce sont bien des
                valeurs numériques avant de les comparer */
                if (is_numeric($_GET["moiscourant"]) && is_numeric($_GET["anneecourante"]))
                {
                        /* L'heure UNIX est codée en secondes sur 32 bits, on ne peut
                         * représenter que des dates allant du 13 décembre 1901 au 19 
                         * janvier 2038. Ne seront affichés que les mois allant
                         * de janvier 1902 à décembre 2037.
                         * Si l'utilisateur saisit dans l'URL une valeur numérique hors 
                         * limite (un mois n'allant pas de 1 à 12 ou une année allant 
                         * au-delà des limites de l'heure UNIX), il sera redirigé vers la
                         * vue mensuelle du mois actuel. */
                        if ($_GET["moiscourant"] > 0 && $_GET["moiscourant"] < 13 && $_GET["anneecourante"] > 1901 && $_GET["anneecourante"] < 2038)
                        {
                                $moisCourantINT   = $_GET["moiscourant"];
                                $anneeCouranteINT = $_GET["anneecourante"];
                        }
                        else
                        {
                                header('Location: mois.php');
                                exit();
                        }
                }
                /* S'il ne s'agit pas de valeurs numériques, retour au mois actuel */
                else
                {
                        header('Location: mois.php');
                        exit();
                }
        }
        /* Si aucun paramètre n'est renseigné, affiche le mois actuel */
        else
        {
                $moisCourantINT   = date('n');
                $anneeCouranteINT = date('Y');
        }

        /* Variables qui vont servir à calculer le nombre et la numérotation des  
         * cases. 
         * On trouve le dernier jour du mois grâce à une propriété de la méthode 
         * mktime() : le jour 0 du mois passé en paramètre correspond au dernier 
         * jour du mois précédent. mktime() retourne un timestamp au format UNIX.
         * date() convertit en string le timestamp. En fonction du caractère passé 
         * en paramètre il affiche le nombre de jours dans le mois ("t"), le numéro 
         * du mois (de 1 pour janvier à 12 pour décembre, "n"), l'année ("Y")...
         * Enfin strtotime permet d'ajouter ou de soustraire à un timestamp UNIX du
         * temps, ici une journée. */
        $premierJourDuMoisUNIX          = mktime(0, 0, 0, $moisCourantINT, 1, $anneeCouranteINT);
        $dernierJourDuMoisINT           = intval(date('t', $premierJourDuMoisUNIX));
        $dernierJourDuMoisUNIX          = mktime(0, 0, 0, $moisCourantINT, $dernierJourDuMoisINT, $anneeCouranteINT);
        $dernierJourDuMoisPrecedentUNIX = mktime(0, 0, 0, $moisCourantINT, 0, $anneeCouranteINT);
        $dernierJourDuMoisPrecedentINT  = date('t', $dernierJourDuMoisPrecedentUNIX);
        $premierJourDuMoisSuivantUNIX   = strtotime("+1 day", $dernierJourDuMoisUNIX);
        $moisPrecedentINT               = date('n', $dernierJourDuMoisPrecedentUNIX);
        $moisSuivantINT                 = date('n', $premierJourDuMoisSuivantUNIX);
        $anneeDuMoisPrecedentINT        = date('Y', $dernierJourDuMoisPrecedentUNIX);
        $anneeDuMoisSuivantINT          = date('Y', $premierJourDuMoisSuivantUNIX);
        $semaine                        = array(
                        'Lundi',
                        'Mardi',
                        'Mercredi',
                        'Jeudi',
                        'Vendredi',
                        'Samedi',
                        'Dimanche'
        );
        $mois                           = array(
                        1 => "Janvier",
                        2 => "Février",
                        3 => "Mars",
                        4 => "Avril",
                        5 => "Mai",
                        6 => "Juin",
                        7 => "Juillet",
                        8 => "Août",
                        9 => "Septembre",
                        10 => "Octobre",
                        11 => "Novembre",
                        12 => "Décembre"
        );

        /* Requête pour récupérer les évenements du mois et ceux des quelques jours 
         * les précédant et les suivant.
         * Initialisation du tableau où seront rangés les événements qui ont lieu
         * entre $dateDebut et $dateFin.*/
        $dateDebut = $anneeDuMoisPrecedentINT . "-" . $moisPrecedentINT . "-22";
        $dateFin   = $anneeDuMoisSuivantINT . "-" . $moisSuivantINT . "-06";
        include("connecteur.php");
        $evenementsDuMois = SelectEvents($_SESSION['agendaVu'], $dateDebut, $dateFin);

        /* signale au header et au footer que la page courante est la vue mois */
        $pageCourante = 'mois';
        include 'header.php';

        /* Si la requête retourne au moins un résultat : 
         * on range chaque événement dans le tableau sous la forme
         * clé = 'mm-jj' et valeur = "Heure et intitulé court de l'évenement".
         * ex : un événement intitulé "RDV Dentiste" qui a lieu le 12 avril 2015 à 
         * 14h a pour clé '2015-04-12' et pour valeur "14:00 RDV Dentiste".
         * Pour récupérer certaines valeurs on utilise substr() qui permet de
         * récupérer une portion de texte dans une chaîne de caractère.
         * Au cas où il y aurait plusieurs événements prévus sur la même journée
         * un message "x événements prévus" est affiché. */
        if (count($evenementsDuMois) > 0)
        {
                $listeEvenements = array();
                $nbEvents        = array();
                foreach ($evenementsDuMois as $evenement)
                {
                        $heureEvent = substr($evenement["eve_debut"], 11, 5);
                        $event      = $heureEvent . " " . $evenement["eve_lib"];
                        $jourEvent  = substr($evenement["eve_debut"], 0, 10);
                        if (!isset($listeEvenements[$jourEvent]))
                        {
                                $listeEvenements[$jourEvent] = substr($event, 0, 30);
                                $nbEvents[$jourEvent]        = 1;
                                $idEvent[$jourEvent][]       = $evenement["eve_id"];
                        }
                        else
                        {
                                $nbEvents[$jourEvent] += 1;
                                $listeEvenements[$jourEvent] = $nbEvents[$jourEvent] . " événements prévus";
                        }
                }
        }
        ?>
            <div id="wrapper">
            <div id="sousHeader">
                <table class="sousHeader">
                    <tr>
                        <td class="left">
                                <?php
        if (!($moisCourantINT == 1 && $anneeCouranteINT == 1902))
        { ?>
                    <a href="mois.php?moiscourant=<?php
                        echo $moisPrecedentINT;
        ?>&anneecourante=<?php
                        echo $anneeDuMoisPrecedentINT;
        ?>"><div id="boutonPrecedent" class="bouton"><<</div></a> 
        <?php
        }
        if (!($moisCourantINT == 12 && $anneeCouranteINT == 2037))
        { ?>
                    <a href="mois.php?moiscourant=<?php
                        echo $moisSuivantINT;
        ?>&anneecourante=<?php
                        echo $anneeDuMoisSuivantINT;
        ?>"><div id="boutonSuivant" class="bouton">>></div></a> 
        <?php
        }
        if (strlen($moisCourantINT) == 2)
        {
                $dateCourante = $anneeCouranteINT . "-" . $moisCourantINT . "-01";
        }
        else
        {
                $dateCourante = $anneeCouranteINT . "-0" . $moisCourantINT . "-01";

        }
        ?>
                        </td>
                        <td>
                            <div id="titreMois"><?php echo $mois[$moisCourantINT] . " " . $anneeCouranteINT; ?></div>        
                        </td>
                        <td class="right">
                            <div id="vue">Vue :</div>
                            <div id="boutonJour" class="bouton"><a href="jour.php?datecourante=<?php echo $dateCourante; ?>" >Jour</a></div>
                            <div id="boutonSemaine" class="bouton"><a href="semaine.php?datecourante=<?php echo $dateCourante; ?>" >Semaine</a></div>
                            <div id="boutonMois" class="bouton">Mois</div>
                        </td>
                    </tr>
                </table>
            </div>        

            <table id="agendaMois">
                <tbody>
                    <tr>
                        <?php
        foreach ($semaine as $jourDeLaSemaine)
        { ?>
                        <td>
                    <?php echo $jourDeLaSemaine;?>
                        </td>
<?php   } ?>
                    </tr>
                    <tr>
                        <?php
        /* Si le 1er du mois courant n'est pas un lundi ("Mon" pour Monday) :
         *  on affiche les jours de la semaine précédant le 1er du mois courant */
        $nbJoursDuLundiAuPremier = 0;
        if (date("D", $premierJourDuMoisUNIX) != "Mon")
        {
                $lundi = false;
                while (!$lundi)
                {
                        if (date("D", $dernierJourDuMoisPrecedentUNIX) == "Mon")
                        {
                                $nbJoursDuLundiAuPremier++;
                                $lundi = true;
                                break;
                        }
                        else
                        {
                                $dernierJourDuMoisPrecedentUNIX = strtotime("-1 day", $dernierJourDuMoisPrecedentUNIX);
                        }
                        $nbJoursDuLundiAuPremier++;
                }
        }

        /* Affiche les derniers jours du mois précédent si le 
         * mois ne commence pas un lundi, puis les jours du mois 
         * en cours et enfin les premiers jours du mois suivant 
         * si le mois en cours ne se termine pas un dimanche.
         * La variable $numeroteurMoisINT numérote les cases. */
        $numeroteurMoisINT = $dernierJourDuMoisPrecedentINT - $nbJoursDuLundiAuPremier + 1;

        for ($i = 0; $i < $nbJoursDuLundiAuPremier; $i++)
        {
                /* L'affichage des événements se fait en vérifiant pour
                chaque date s'il y a une clé correspondante dans la liste
                * d'évenements $listeEvent. La clé prend la forme mm-jj.
                * Les zéros non significatifs ne sont pas conservés en 
                * INT, ce qui fausse le résultat 
                */
                if (strlen($moisCourantINT) == 1)
                {
                        if (strlen($numeroteurMoisINT) == 1)
                        {
                                $cleEvent = $anneeDuMoisPrecedentINT . "-0" . $moisPrecedentINT . "-0" . $numeroteurMoisINT;
                        }
                        else
                        {
                                $cleEvent = $anneeDuMoisPrecedentINT . "-0" . $moisPrecedentINT . "-" . $numeroteurMoisINT;
                        }
                }
                else if (strlen($numeroteurMoisINT) == 1)
                {
                        $cleEvent = $anneeDuMoisPrecedentINT . "-" . $moisPrecedentINT . "-0" . $numeroteurMoisINT;
                }
                else
                {
                        $cleEvent = $anneeDuMoisPrecedentINT . "-" . $moisPrecedentINT . "-" . $numeroteurMoisINT;
                }   ?>
                        <td class="autrejour" onclick="window.open('evenement.php?date=<?php echo $cleEvent;
?>', 'Ajout Evénement', 'height=450, width=400, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;">
                            <?php
                echo $numeroteurMoisINT;
                if (isset($listeEvenements[$cleEvent]))
                {
?>
            <div class="evenement" onclick="window.open('resume.php?idEvent=<?php
                        echo $idEvent[$cleEvent][0];
?>', 'Ajout Evénement', 'height=450, width=400, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no') "><?php
                        echo $listeEvenements[$cleEvent];?>
            </div>
        <?php   }
        $numeroteurMoisINT++; ?>
                        </td> 
<?php   }
        $numeroteurMoisINT = 1;
        $nbCasesTotal      = $dernierJourDuMoisINT + $nbJoursDuLundiAuPremier;
        for ($i = $nbJoursDuLundiAuPremier + 1; $i <= $nbCasesTotal; $i++)
        {
                if (strlen($moisCourantINT) == 1)
                {
                        if (strlen($numeroteurMoisINT) == 1)
                        {
                                $cleEvent = $anneeCouranteINT . "-0" . $moisCourantINT . "-0" . $numeroteurMoisINT;
                        }
                        else
                        {
                                $cleEvent = $anneeCouranteINT . "-0" . $moisCourantINT . "-" . $numeroteurMoisINT;
                        }
                }
                else if (strlen($numeroteurMoisINT) == 1)
                {
                        $cleEvent = $anneeCouranteINT . "-" . $moisCourantINT . "-0" . $numeroteurMoisINT;
                }
                else
                {
                        $cleEvent = $anneeCouranteINT . "-" . $moisCourantINT . "-" . $numeroteurMoisINT;
                }   ?>
                <td class="jourcourant" onclick="window.open('evenement.php?date=<?php
                echo $cleEvent;
?>', 'Ajout Evénement', 'height=450, width=400, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;">
                    <div class="case">
                        <?php echo $numeroteurMoisINT;
                if (isset($listeEvenements[$cleEvent]))
                {
?><div class="evenement" onclick="window.open('resume.php?idEvent=<?php
                        echo $idEvent[$cleEvent][0];
?>', 'Ajout Evénement', 'height=450, width=400, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no') "><?php
                        echo $listeEvenements[$cleEvent];
?></div><?php
                }
                $numeroteurMoisINT++;
                $dernierJour = $i % 7;
?></div></td>
                <?php
                if ($i % 7 == 0)
                {
?></tr><tr>
            <?php
                }
        }

        if (date("D", $premierJourDuMoisUNIX) != "Mon")
        {
                $lundi = false;
                while (!$lundi)
                {
                        if (date("D", $dernierJourDuMoisPrecedentUNIX) == "Mon")
                        {
                                $lundi = true;
                                break;
                        }
                        else
                        {
                                $dernierJourDuMoisPrecedentUNIX = strtotime("-1 day", $dernierJourDuMoisPrecedentUNIX);
                                echo date("D", $dernierJourDuMoisPrecedentUNIX);
                        }
                        $nbJoursDuLundiAuPremier++;
                }
        }

        if ($dernierJour != 0)
        {
                $numeroteurMoisINT = 1;
                while ($dernierJour != 7)
                { 
                        $jourDuMoisSuivantUNIX = mktime(0, 0, 0, $moisSuivantINT, $numeroteurMoisINT, $anneeDuMoisSuivantINT);
                        if (strlen($moisSuivantINT) == 1)
                        {
                                if (strlen($numeroteurMoisINT) == 1)
                                {
                                        $cleEvent = $anneeDuMoisSuivantINT . "-0" . $moisSuivantINT . "-0" . $numeroteurMoisINT;
                                }
                                else
                                {
                                        $cleEvent = $anneeDuMoisSuivantINT . "-0" . $moisSuivantINT . "-" . $numeroteurMoisINT;
                                }
                        }
                        else if (strlen($numeroteurMoisINT) == 1)
                        {
                                $cleEvent = $anneeDuMoisSuivantINT . "-" . $moisSuivantINT . "-0" . $numeroteurMoisINT;
                        }
                        else
                        {
                                $cleEvent = $anneeDuMoisSuivantINT . "-" . $moisSuivantINT . "-" . $numeroteurMoisINT;
                        }
?>                    

                <td class="autrejour" onclick="window.open('evenement.php?date=<?php echo $cleEvent;
?>', 'Ajout Evénement', 'height=450, width=400, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"><?php
                        echo $numeroteurMoisINT;

                        if (isset($listeEvenements[$cleEvent]))
                        {
?><div class="evenement" onclick="window.open('resume.php?idEvent=<?php
                                echo $idEvent[$cleEvent][0];
?>', 'Ajout Evénement', 'height=450, width=400, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no') "><?php
                                echo $listeEvenements[$cleEvent];
                        }
                        $numeroteurMoisINT++;
                        $dernierJour++;?>
                </td>
    <?php       }
        }   ?>
                </tbody>
            </table>
        <?php
        include('footer.php');
        ?>