<?php
/**
 * reinit.php - Page de traitement. Met à jour le mot de passe de l'utilisateur. 
 */
session_start();
/* Si l'utilisateur n'est pas connecté, retour en page d'accueil */
if(!isset($_SESSION['admin']))
{
        header( 'Location: index.php?erreurDroits=1');
        exit();
}
/* Sinon la page s'affiche aux conditions suivantes :
 * - l'utilisateur est connecté en tant qu'administrateur et a cliqué sur 
 * OK pour réinitialiser le mot de passe d'untel
 * - l'utilisateur a renseigné son mot de passe actuel et le nouveau, deux fois
 * et a cliqué su OK */
else if ((isset($_SESSION['admin']) && $_SESSION['admin'] == 1 && isset($_POST['reinit']) || (isset($_POST['mdpactuel']) && isset($_POST['mdpnouveau']) && isset($_POST['mdpverif'])))) 
{
     include('connecteur.php'); ?>
    <html>
    <head>
            <meta charset="utf-8">
    </head>
<?php
    /* L'utilisateur a renseigné les trois champs nécéssaires pour son changement de mot de passe */
    if (isset($_POST['mdpactuel']) && isset($_POST['mdpnouveau']) && isset($_POST['mdpverif'])) 
    {
            /* Si l'un des champs est vide, retour à la page avec un message d'erreur "Champ vide" */
            if ($_POST['mdpactuel'] == '' || $_POST['mdpnouveau'] == '' || $_POST['mdpverif'] == '') 
            {
                    header('Location: options.php?reinit=5');
                    exit();
            }
            /* Sinon on regarde si l'utilisateur correspond bien au mot 
             * de passe actuel.
             */
            else {
                    $requete = SelectAll($_SESSION["login"], $_POST['mdpactuel']);
                    /* Si le mot de passe ne correspond pas, retour aux options + erreur */
                    if (sizeof($requete) == 0) 
                    {
                            header('Location: options.php?reinit=2');
                            exit();
                    } 
                    /* Si les deux nouveaux mots de passe ne sont pas identiques, 
                     * retour aux options + erreur */
                    else if ($_POST['mdpnouveau'] != $_POST['mdpverif']) 
                    {
                            header('Location: options.php?reinit=3');
                            exit();
                    }
                    /* Sinon tout est bon, on envoie le nouveau mot de passe à
                     * la base. */
                    else 
                    {
                            $reussi = ReinitialisationMdp($_SESSION["id"], $_POST['mdpnouveau']);
                            if ($reussi) 
                            {
                                header('Location: options.php?reinit=4');
                            } 
                            else 
                            {
                                echo "Erreur à l'insertion.";
                            }
                    }
            }
    }
    /* L'utilisateur est un admin et a séléctionné la personne dont il veut 
     * réinitialiser le mot de passe : un mot de passe est généré aléatoirement
     * et transmis à l'utilisateur concerné. Puis retour aux options. */
    else if (isset($_SESSION['admin']) && $_SESSION['admin'] == 1 && isset($_POST['reinit'])) 
    {
            $nouveauMdp   = GenerationMdp();
            $idReinit     = substr($_POST["reinit"], 0, 1);
            $idReinit     = intval($idReinit);
            $destinataire = substr($_POST["reinit"], 1);
            $reussi       = ReinitialisationMdp($idReinit, $nouveauMdp);
            if ($reussi) 
            {
                    EnvoiMessage($destinataire, $nouveauMdp);
                    header('Location: options.php?reinit=1');
            }
            else 
            {
                    echo "Erreur à l'insertion.";
            }
    }
}
/* Dans les autres cas, retour à la vue précédente */
else
{
        header('Location: index.php');
        exit();
}
?>
    </html>
 <?php
 /**
  * Génère aléatoirement un mot de passe contenant au moins une majuscule,
  * des chiffres et des caractères spéciaux.
  * @return string
  */
function GenerationMdp()
{
    $caracteres = 'abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789,;:!?./§$*#'; //pas de l, trop similaire au I
    $majuscules = 'ABCDEFGHJKLMNOPQRSTUVWXYZ'; //pas de I, trop similaire au l
    $chiffres   = '0123456789';
    $caraspec   = ',:!?./§$*#';
    $caracteres = str_shuffle($caracteres);
    $majuscules = str_shuffle($majuscules);
    $chiffres   = str_shuffle($chiffres);
    $caraspec   = str_shuffle($caraspec);
    $mdp        = substr($caracteres, 0, 8) . substr($majuscules, 0, 1) . substr($chiffres, 0, 1) . substr($caraspec, 0, 1);
    return $mdp;
}
/**
 * Envoie à l'adresse mail de l'utilisateur un mail l'informant de son nouveau
 * mot de passe.
 * @param string $destinataire
 * @param string $nouveauMdp
 */
function EnvoiMessage($destinataire, $nouveauMdp)
{
    $body = "       Bonjour,     
                Votre administrateur a procédé à la réinitialisation de votre mot de passe.
                Votre nouveau mot de passe est : $nouveauMdp 
                Nous vous recommandons de changer de mot de passe immédiatement après vous être reconnecté, dans le menu des options (clic sur votre nom dans la barre latérale).
                En vous remerciant de votre confiance,
                Nadine-Van";
    ini_set('sendmail_from', 'contact@sio2015.fr');
    mail($destinataire, 'Réinitialisation du mot de passe', $body, "From: contact@sio2015.fr");
}
?>