<?php
/**
 * traitement.php - Page de traitement. Modifie ou supprime un événement de la base.
 */

session_start();
if (!isset($_SESSION['login'])) 
{
        header('Location: index.php?erreurDroits=1');
        exit();
}
?>
<!DOCTYPE html>

<html>
    <head>
	<title>Calendrier M2L</title>
	<meta charset="utf-8">
    </head>

<?php
include("connecteur.php");
if (isset($_POST['actiontype']) && isset($_POST['idEvent']) && $_POST['actiontype'] == "delete") 
{
        $reussi = DeleteEvent($_POST['idEvent']);
        if ($reussi) 
        {
                echo "L'événement a bien été supprimé."; ?>
                    <input type="button" value="fermer" onclick="javascript:window.close();">
                    <script type="text/javascript">parent.opener.location.reload();</script><?php
        }
        exit();
}
/* Si l'intitulé est disponible le formulaire a forcément été renseigné. */
if (isset($_POST["intitule"])) 
{
        $dateDebut     = $_POST["dateDebut"] . " " . $_POST["heureDebut"] . ":" . $_POST["minutesDebut"] . ":00.000000";
        $dateFin       = $_POST["dateFin"] . " " . $_POST["heureFin"] . ":" . $_POST["minutesFin"] . ":00.000000";
        $dateTimeDebut = new DateTime($dateDebut);
        $dateTimeFin   = new DateTime($dateFin);
        /* S'il y a une incohérence sur les dates (l'heure de fin précède l'heure de début), retour au formulaire
         * avec les informations déjà renseignées et un message d'erreur */
        if ($dateTimeFin < $dateTimeDebut) 
        {
                header('Location: evenement.php?date=' . $_POST["dateDebut"] . '&heure=' . $_POST["heureDebut"] . '&minutes=' . $_POST["minutesDebut"] . '&intitule="' . $_POST["intitule"] . '"&description=' . $_POST["description"] . '&etatDispo=' . $_POST["etatDispo"] . '&erreurDate=1&actiontype=' . $_POST["actiontype"] . "&idEvent=" . $_POST['idEvent']);
                exit();
        }
        $intitule    = strip_tags($_POST["intitule"]);
        $description = strip_tags($_POST["description"]);
        $etatDispo   = intval($_POST["etatDispo"]);
        if (isset($_POST['idEvent'])) 
        {
                $id = $_POST['idEvent'];
        }
        else if (isset($_GET['idEvent'])) 
        {
                $id = $_GET['idEvent'];
        } 
        else 
        {
                $id = intval($_SESSION["id"]);
        }

        /* pour les événements sur plusieurs jours : on les tronçonne en plusieurs
         * événements du même nom qui durent toute la journée.
         */
        if ($_POST["dateDebut"] != $_POST["dateFin"]) 
        {
                $dateTimeDebut  = new DateTime($dateDebut);
                $dateTimeFin    = new DateTime($dateFin);
                $nbJours        = $dateTimeDebut->diff($dateTimeFin);
                $nbJours        = ($nbJours->d) + 1; //ce compteur permet de compter les jours entre la date de début et la date de fin
                $datePrecedente = $dateDebut;
                $dateSuivante   = $dateTimeDebut->format('y-m-d 23:59:59');
                $i              = 0;
                $reussi         = true;
                if (isset($_POST['actiontype']) && $_POST['actiontype'] != "modifier") 
                {
                        while ($reussi && $i < $nbJours) 
                        {
                                $reussi         = InsertEvent($intitule, $description, $datePrecedente, $dateSuivante, $etatDispo, $id);
                                $datePrecedente = new DateTime($dateSuivante);
                                $dateSuivante   = new DateTime($dateSuivante);
                                $datePrecedente = $dateSuivante->modify('+1 second');
                                $dateSuivante   = $datePrecedente->format('y-m-d 23:59:59');
                                $datePrecedente = $datePrecedente->format('y-m-d H:i:s');
                                $i++;
                        }
                        $reussi = InsertEvent($intitule, $description, $datePrecedente, $dateFin, $etatDispo, $id);
                }
        } 
        else if (isset($_POST['actiontype']) && $_POST['actiontype'] == "modifier") 
        {
                $reussi = UpdateEvent($intitule, $description, $dateDebut, $dateFin, $etatDispo, $id);
        } 
        else 
        {
                $reussi = InsertEvent($intitule, $description, $dateDebut, $dateFin, $etatDispo, $id);
        }
        
        if ($reussi == 1) 
        {
                echo "L'événement a bien été enregistré"; ?>
                <input type="button" value="fermer" onclick="javascript:window.close();">
                <script type="text/javascript">parent.opener.location.reload();</script>
                    <?php
        }
        else 
        {
                echo "Erreur de traitement.";
        }
}
?>
