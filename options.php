<?php 
/**
 * options.php - Menu des options. Propose de changer ou de réinitialiser un
 * mot de passe, ou de charger une sauvegarde/un dump de la base de données.
 */
    session_start();
    /* L'utilisateur n'est pas connecté : il retourne en page d'accueil */
    if (!isset($_SESSION['admin']))
    {
            header('Location: index.php?erreurDroits=1');
            exit();
    }
    /* L'utilisateur est connecté : il voit la page avec les options de base
     * qu'il soit admin ou pas. */
    else if (isset($_SESSION['admin']))
    {
        include ('connecteur.php');
    ?>
    <!DOCTYPE html>
    <html>
    <head>
            <title>Options</title>
                    <meta charset="utf-8">
                    <link href="css/style.css" rel='stylesheet' type='text/css' />
                    <!--Webfonts-->
                    <link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,300,600,700' rel='stylesheet' type='text/css'>
                    <!--Webfonts-->
    </head>
    <body>
            <div id="conteneur-options">
                    <div id='options-gauche'>
                            <p><a href="index.php" ><< Retour à l'agenda</a></p>
                            <h1>Options générales</h1>
                            <h2>Informations du compte</h2>
                            <p>Adresse mail : <?php echo $_SESSION['login']; ?></p>
                            <form class="formulaire" method='POST' action="reinit.php">
                                    <fieldset>
                                        <legend>Changement de mot de passe</legend>
                                            <p>
                                                <label for="mdpactuel">Mot de passe actuel : </label>
                                                <input type="password" name="mdpactuel" id="mdpactuel" />
                                            </p>
                                            <p>
                                                <label for="mdpnouveau">Nouveau mot de passe : </label>
                                                <input type="password" name="mdpnouveau" id="mdpnouveau" />
                                            </p>
                                            <p>
                                                <label for="mdpverif">Nouveau mot de passe (pour validation) : </label>
                                                <input type="password" name="mdpverif" id="mdpverif" />
                                            </p>
                                            <?php if(isset($_GET["reinit"]) && $_GET["reinit"] == 2){ ?>Mot de passe actuel erroné<?php } ?>
                                            <?php if(isset($_GET["reinit"]) && $_GET["reinit"] == 3){ ?>Saisie différente<?php } ?>
                                            <?php if(isset($_GET["reinit"]) && $_GET["reinit"] == 4){ ?>Le mot de passe a bien été modifié.<?php } ?>
                                            <?php if(isset($_GET["reinit"]) && $_GET["reinit"] == 5){ ?>Tous les champs n'ont pas été renseignés.<?php } ?>
                                            <p>
                                                <input type="submit" value="OK" />
                                            </p>
                                    </fieldset>
                            </form>
                    
                            <p>Exporter son agenda :</p>
                            <form class="formulaire" method='POST' action="export.php">
                                    <input type="hidden" name="exportID" id="exportID" value="<?php echo $_SESSION['id'];?>" />
                                    <input type="submit" value="OK" />
                            </form>

                            <p>Importer son agenda (format .wgd) :</p>
                            <form class="formulaire" method='POST' action="export.php" enctype="multipart/form-data">
                                    <input type="file" name="upload" id="upload" />
                                    <input type="hidden" name="MAX_FILE_SIZE" value="12345" />
                                    <input type="hidden" name="importID" id="importID" value="<?php echo $_SESSION['id'];?>" />
                                    <input type="submit" value="OK" />
                            </form>
                             <?php if(isset($_GET["error"]) && $_GET["error"] == 1){ ?>Le fichier envoyé n'est pas au format .wgd<?php }
                             else if(isset($_GET["error"]) && $_GET["error"] == 0){ ?>La sauvegarde a bien été restaurée.<?php }
                             else if(isset($_GET["error"]) && $_GET["error"] == 2){ ?>La sauvegarde n'a pu être restaurée.<?php } ?>
                    </div>                      
                    <?php    
                    /* L'utilisateur est connecté en tant qu'admin : il voit des options
                     * d'administration supplémentaires */
                    if ($_SESSION['admin']==1){?>
                    <div id='options-droite'>
                            <h1>Options d'administration</h1>
                            
                             <p>Exporter la base de données :</p>
                    <form class="formulaire" method='POST' action="export.php">
                            <input type="hidden" name="dump" id="dump" value="<?php echo $_SESSION['id'];?>" />
                            <input type="submit" value="OK" />
                    </form>
                            <h2>Réinitialisation de mot de passe</h2>
                            
                            <form class="formulaire" method='POST' action="reinit.php">
                                Réinitialiser le mot de passe de : 
                                    <select name="reinit">
                                            <?php $listeCollegues = SelectCollegues();
                                            foreach($listeCollegues as $collegue){?>
                                            <option value="<?php echo $collegue["uti_id"].$collegue["uti_log"] ?>" <?php if($_SESSION['agendaVu'] == $collegue["uti_id"]){ echo 'selected';} ?>><?php echo $collegue["uti_pre"]." ".$collegue["uti_nom"]; ?></option>
                                            <?php } ?>
                                    </select>
                                <input type="submit" value="OK" />
                                <?php if(isset($_GET["reinit"]) && $_GET["reinit"] == 1){ ?>Le mot de passe a bien été réinitialisé.<?php } ?>
                            </form>
                            <p><a href="index.php" ><< Retour à l'agenda</a></p>
                    </div>
                    <?php } ?>
            </div>
    </body>
    </html>
<?php }?>