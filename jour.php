<?php
/**
 * jour.php - Affiche le planning d'un jour de la semaine
 */

session_start();
/* si l'utilisateur n'est pas logué : retour à index.php */
if (!isset($_SESSION['login']))
{
        header('Location: index.php?erreurDroits=1');
        exit();
}
$_SESSION['vue'] = 1; //1 correspond à la vue en cours (ici jour)
?>
<!DOCTYPE html>
<html>
<head>
	<title>Agenda M2L - Vue hebdomadaire</title>
        <?php
        /* chargement du head et des fonctions de connexion à la base de données */
        include 'header.php';
        include 'connecteur.php';

        /* signalement au header et au footer que la page courante est la vue jour */
        $pageCourante = 'jour';

        /* initialisation d'une date au fuseau horaire UTC +1 */
        try
        {
                date_default_timezone_set('Europe/Paris'); //Définition du fuseau horaire par défaut, tant pis pour les utilisateurs étrangers :'D
        }
        catch (Exception $e)
        {
                exit($e->getMessage());
        }
        $maintenant = new DateTime();

        /* Si une date est spécifiée en paramètre dans l'URL :
         * récupère cette valeur et crée un DateTime $dateCourante 
         * après vérification du format de cette valeur.
         * preg_match() vérifie que la chaîne respecte bien le format 
         * spécifié par la regex (YYYY-MM-DD), la découpe et l'affecte
         * à un tableau (ici $split). Ensuite il teste si cette valeur
         * permet de créer une date grâce à checkdate();
         * Sinon par défaut il affecte la valeur du jour courant */
        if (isset($_GET['datecourante']) && preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $_GET['datecourante'], $split) && checkdate($split[2], $split[3], $split[1]))
        {
                $dateCourante = new DateTime($_GET['datecourante']);
        }
        else
        {
                $dateCourante = $maintenant;
        }
        /* Récupération de toutes les valeurs nécéssaires à l'affichage 
         * du calendrier du jour */
        $jourCourant     = $dateCourante->format("N"); //Récupère le numéro du jour dans la semaine (de 1 pour lundi à 7 pour dimanche)
        $numeroJour      = $dateCourante->format('d'); //Récupère le numéro du jour (le 1er avril = 1, le 25 décembre = 25...)
        $moisCourant     = $dateCourante->format("n"); //Récupère le numéro du mois sans les zéros initiaux
        $anneeCourante   = $dateCourante->format("Y"); //Récupère l'année complète
        $semaineCourante = $dateCourante->format("W"); //Récupère le numéro de la semaine
        $heureActuelle   = $maintenant->format("H"); //Récupère l'heure actuelle

        $dateCourante2 = new DateTime($dateCourante->format("Y-m-d")); //Création d'une $dateCourante2 égale à $dateCourante dans le but pouvoir le modifier en gardant $dateCourante inchangé
        $lendemain     = new DateTime($dateCourante2->modify('+1 day')->format("Y-m-d"));
        $lendemain     = $lendemain->format("Y-m-d");
        $veille        = new DateTime($dateCourante2->modify('-2 day')->format("Y-m-d"));
        $veille        = $veille->format("Y-m-d");
        $dateCourante2 = new DateTime($dateCourante2->modify('+1 day')->format("Y-m-d"));
        $dateCourante2 = $dateCourante2->format("Y-m-d");

        $jour  = array(
                        '',
                        'Lundi',
                        'Mardi',
                        'Mercredi',
                        'Jeudi',
                        'Vendredi',
                        'Samedi',
                        'Dimanche'
        );
        $mois  = array(
                        '',
                        'janvier',
                        'février',
                        'mars',
                        'avril',
                        'mai',
                        'juin',
                        'juillet',
                        'août',
                        'septembre',
                        'octobre',
                        'novembre',
                        'décembre'
        );
        /* Boucle permettant de lister toutes les heures de 00:00 à 23:00 */
        $heure = array();
        for ($i = 0; $i < 24; $i++)
        {
                        if ($i < 10)
                        {
                                $heure[] = '0' . $i . ':00';
                        }
                        else
                        {
                                $heure[] = $i . ':00';
                        }
        }
        /* Appel de la fonction SelectEvents() pour récupérer les événements 
         * ayant lieu dans la journée.
         */
        $listeEvenementsDuJour = SelectEvents($_SESSION['agendaVu'], $dateCourante2 . " 00:00:00", $lendemain . "  00:00:00");
        
        /* Si au moins un événement a lieu ce jour-là : */
        if (count($listeEvenementsDuJour) > 0)
        {
                    /* Le tableau $ecriture[] sert à stocker l'état d'écriture d'un évènement.
                     * Lors de l'affichage d'un événement on affiche l'intitulé sur la première
                     * ligne puis l'heure de début et l'heure de fin sur la seconde (s'il y en
                     * a une). On initialise donc le contenu à 'intitulé' puisque c'est celui 
                     * qui s'affiche en premier. Cette valeur sera amenée à changer en fonction
                     * de la durée de l'événement pour afficher l'événement sur toutes les
                     * cases concernées.
                     */
                    for ($i = 0; $i < count($listeEvenementsDuJour); $i++)
                    {
                            $ecriture[$i]        = 'intitule';
                            $listeInchangee[$i]  = $listeEvenementsDuJour[$i];
                    }
        }
        /* Affichage de la barre de navigation du header */
        ?>
            <div id="wrapper">
                <div id="sousHeader">
                        <table class="sousHeader">
                            <tr>
                                <td class="left">
                                    <a href="jour.php?datecourante=<?php echo $veille;?>">
                                        <div id="boutonPrecedent" class="bouton"><<</div>
                                    </a>
                                    <a href="jour.php?datecourante=<?php echo $lendemain;?>">
                                        <div id="boutonSuivant" class="bouton">>></div>
                                    </a>
                                </td>
                                <td id="titreJour">
                                    <div>
                                        <?php echo "$jour[$jourCourant] $numeroJour $mois[$moisCourant] $anneeCourante";?>
                                    </div>        
                                </td>
                                <td class="right">
                                    <div id="vue">Vue :</div>
                                    <div id="boutonJour" class="bouton">Jour</div>
                                    <div id="boutonSemaine" class="bouton"><a href="semaine.php?datecourante=<?php echo $dateCourante2; ?>">Semaine</a></div>
                                    <div id="boutonMois" class="bouton"><a href="mois.php?moiscourant=<?php echo $moisCourant; ?>&anneecourante=<?php echo $anneeCourante;?>">Mois</a></div>
                                </td>
                            </tr>
                        </table>
                </div>
        <?php /* Affichage des entêtes de la table */ ?>
        <table id="enteteAgendaJour">
                                <tr>
                                        <th id="heureTH"></th>
                                        <th id="eventsTH"><?php
        echo "$jour[$jourCourant] $numeroJour $mois[$moisCourant] $anneeCourante";
        ?></th>
                                </tr>
        </table>
                <div id="agendaSemaine">
                        <table id="tableAgendaSemaine" border="1">
                        <?php
        /* Construction des 24 <tr> permettant la division horaire d'une journée
         * La colonne .heure spécifie l'heure correspondant à la ligne.
         * rowspan permet de spécifier sur combien de lignes porte la case horaire,
         * ici 2, ce qui permet de fractionner l'heure en deux demi-heure et donc 
         * d'avoir des plages horaires en xx:00 et en xx:30.*/
        for ($i = 0; $i < 24; $i++)
        {
                /* Deux boucles pour chaque heure : une pour la première demi-heure
                une pour la seconde*/
                for ($e = 0; $e < 2; $e++)
                {   ?>
                <tr>      
                        <?php
                        if ($e == 0)
                        {   ?>
                    <th class="heure" id="<?php
                                    if ($i == $heureActuelle && $maintenant->format("W") == $semaineCourante)
                                    {
                                            echo "current";
                                    }
                                    else
                                    {
                                            echo "thRow" . $i;
                                    }
?>" rowspan="2"><?php
                                    echo "$heure[$i]";
?></th>             
                <?php   }
                        /* Affichage des cellules d'une heure donnée.
                         * Au clic, appelle un formulaire de création d'événement 
                         * et lui transmet le jour, la date et l'heure de la cellule.
                         * Vérifie s'il y a des événements à afficher ce jour
                         * si c'est le cas, regarde dans cette liste si l'heure 
                         * de début d'un événement correspond au jour et à l'heure 
                         * de la cellule en cours de création, et enfin si c'est 
                         * le cas crée dans la cellule une div où s'affiche 
                         * l'intitulé ou l'heure de l'événement */ ?>
                        <td class="events" 
                            id="r<?php
                        echo $i;
?>c0" 
                            onclick="window.open('evenement.php?date=<?php
                        if (strlen($moisCourant) == 1)
                        {
                                $moisCourant = '0' . $moisCourant;
                        }
                        if (strlen($numeroJour) == 1)
                        {
                                $numeroJour = '0' . $numeroJour;
                        }
                        echo $anneeCourante . '-' . $moisCourant . '-' . $numeroJour;
?>&heure=<?php
                        if (strlen($i) == 2)
                        {
                                echo $i;
                        }
                        else
                        {
                                echo "0" . $i;
                        }
?>&minutes=<?php        if ($e == 0)
                        {
                                echo '00';
                        }
                        else
                        {
                                echo '30';
                        }
?>', 'Ajout Evénement', 'height=450, width=400, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;">
                        <?php
                        //vérifie l'existence d'événements pour la journée
                        if (count($listeEvenementsDuJour) > 0)
                        {
                                /* détermine la durée de l'événement et l'heure à afficher dans la cellule */

                                for ($c = 0; $c < count($listeEvenementsDuJour); $c++)
                                {
                                        $heureDebut    = substr($listeInchangee[$c]['eve_debut'], 11, 5);
                                        $heureFin      = substr($listeInchangee[$c]['eve_fin'], 11, 5);
                                        $heureAffichee = $heureDebut . " - " . $heureFin;

                                        /*Si le jour et l'heure de la cellule correspondent bien à ceux de l'événement,
                                         * On affiche l'événement. Ici trois cas de figure sont possibles :
                                         * - l'événement commence à la demie (xx:30) et dure au moins une demie-heure
                                         * - l'événement commence à l'heure pile (xx:00) et dure au moins une heure
                                         * - l'événement dure une demie-heure et finit à la demie. 
                                         * */
                                        if (substr($listeEvenementsDuJour[$c]['eve_debut'], 8, 2) == $numeroJour && substr($listeEvenementsDuJour[$c]["eve_debut"], 11, 2) == substr($heure[$i], 0, 2))
                                        {
                                                //On fait les différences entre les heures de datefin/datedebut ainsi que celle entre les minutes
                                                $differenceHeures  = substr($listeEvenementsDuJour[$c]['eve_fin'], 11, 2) - substr($listeEvenementsDuJour[$c]['eve_debut'], 11, 2);
                                                $differenceMinutes = substr($listeEvenementsDuJour[$c]['eve_fin'], 14, 2) - substr($listeEvenementsDuJour[$c]['eve_debut'], 14, 2);
                                                $minutesDebut      = substr($listeInchangee[$c]['eve_debut'], 14, 2);
                                                $minutesFin        = substr($listeInchangee[$c]['eve_fin'], 14, 2);

                                                if ($e == 1)
                                                {
                                                        $nouvelleDateDebut = substr($listeEvenementsDuJour[$c]['eve_debut'], 11, 2) + 1;
                                                        //Ajout d'un 0 ou non en fonction de si $nouvelleDateDebut est composé d'un ou de deux chiffres
                                                        if ($nouvelleDateDebut < 10)
                                                        {
                                                                $listeEvenementsDuJour[$c]['eve_debut'] = substr($listeEvenementsDuJour[$c]['eve_debut'], 0, 10) . " 0" . $nouvelleDateDebut . substr($listeEvenementsDuJour[$c]['eve_debut'], 13, 6);
                                                        }
                                                        else
                                                        {
                                                                $listeEvenementsDuJour[$c]['eve_debut'] = substr($listeEvenementsDuJour[$c]['eve_debut'], 0, 10) . " " . $nouvelleDateDebut . substr($listeEvenementsDuJour[$c]['eve_debut'], 13, 6);
                                                        }
                                                }

                                                /* Premier cas : l'événement dure au moins une demi-heure et commence à la demie (xx:30) :
                                                 * La cellule n'affiche rien et $ecriture reste valorisée à 'intitule' pour indiquer à la
                                                 * cellule d'en dessous que c'est à elle d'afficher l'intitule */
                                                if ($minutesDebut == 30 && $ecriture[$c] == 'intitule' && $e == 0)
                                                {

                                                }
                                                /* Deuxième cas : l'événement dure une demi-heure et finit à la demie,
                                                 * ou l'événement dure plus d'une heure.
                                                 * En fonction de ce que lui indique $ecriture elle affiche soit l'intitulé,
                                                 * soit l'heure, soit une case vide (dans tous les cas colorée).
                                                 * Ensuite $ecriture change de valeur pour indiquer à la case d'en dessous
                                                 * d'afficher :
                                                 * - l'heure, si la case en cours a affiché l'intitulé
                                                 * - une case colorée vide si la case est cours a affiché l'heure
                                                 */
                                                else if ($differenceHeures > 0 || ($differenceHeures == 0 && $minutesFin == 30) || $minutesFin == 59)
                                                {
                                                        if ($ecriture[$c] == 'intitule')
                                                        {
?>
                                <div class="evenement" onclick="window.open('resume.php?idEvent=<?php
                                                                echo $listeEvenementsDuJour[$c]['eve_id'];
?>', 'Vue Evénement', 'height=450, width=400, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no')">
                                    <?php                       echo $listeEvenementsDuJour[$c]['eve_lib']; ?>
                                </div>
                                    <?php                       if ($minutesFin == 59)
                                                                {
                                                                        $ecriture[$c] = 'vide';
                                                                }
                                                                else
                                                                {
                                                                        $ecriture[$c] = 'heure';
                                                                }
                                                        }
                                                        else if ($ecriture[$c] == 'heure')
                                                        { ?>
                                <div class="evenement">
                                        <?php                   echo $heureAffichee; ?>
                                </div>
                <?php                                           $ecriture[$c] = 'vide';
                                                        }
                                                        else if ($ecriture[$c] == 'vide')
                                                        { ?>
                                        <div class="evenement noBorderTop">&nbsp;</div>
                <?php
                                                        }
                                                        /* Si l'événement finit dans la première demi-heure, on change son
                                                         * heure de fin pour que l'événement ne s'affiche pas dans la
                                                         * cellule suivante.*/
                                                        if ($differenceHeures == 0 && $minutesFin == 30)
                                                        {
                                                                $listeEvenementsDuJour[$c]['eve_fin'] = 0;
                                                                $ecriture[$c]                         = '';

                                                        }
                                                }
                                        } //fin de la condition "Si l'événement correspond bien à la date et l'heure de cellule"	
                                } //fin de la boucle sur cheque événement de la liste
                        } //fin de la condition "S'il existe au moins un événement pour cette semaine"
?>

                        </td>
                </tr>
        <?php
                } //fin de la construction de la ligne <tr>
        } //boucle deux fois pour construire deux lignes <tr> 
        ?>
                        </table>
                </div>
        <?php
        include('footer.php');
        ?>