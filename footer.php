<?php
/**
 * Footer.php - Pied de page des vues de l'agenda (jour.php, semaine.php et mois.php)
 */
?>
<div id="footer">
        <table>
            <tr>
                <td>
                    <?php
if ($pageCourante == 'semaine') 
{
        echo 'Semaine ' . $semaineCourante;
}
else if ($pageCourante == 'mois') 
{
        echo $mois[$moisCourantINT] . " " . $anneeCouranteINT;
}
?>
                </td>
                <td>Agenda : <form method='POST' action="changementAgenda.php">
                        <select name="agendaVu">
                            <?php
if (isset($_SESSION["collegues"])) 
{
        $listeCollegues = $_SESSION["collegues"];
} 
else 
{
        $listeCollegues = SelectCollegues();
}

foreach ($listeCollegues as $collegue) 
{   ?>
                            <option value="<?php echo $collegue["uti_id"];?>" 
    <?php
    if (isset($_SESSION['agendaVu']) && $_SESSION['agendaVu'] == $collegue["uti_id"]) 
    {
            echo 'selected';
    } ?>>
    <?php echo $collegue["uti_pre"] . " " . $collegue["uti_nom"];?>
                            </option>
<?php
}
?>
                        </select>
                        <input type="submit" value="Voir" />
                    </form>
                </td>
                <td>
                    <form method="POST" action="recherche.php">
                        <input type="text" name="recherche" placeholder="Rechercher un événement" />
                        <input type="submit" value="Rechercher" />
                    </form>
                </td>
                
            </tr>
        </table>
    </div>
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script>
            
            function actuel() {
                document.getElementById( 'current' ).scrollIntoView();
            };
            actuel();
        </script>
        <script>
            jQuery(function($){
    $(".events, .evenement, .resume").click(function(e){
        e.stopPropagation();
        var href = $(this).attr("data-href");
    });
});
        </script>        
</div><!-- --></body><!-- --></html>