<?php
/**
 * recherche.php - Affiche la liste d'événements dont l'intitulé ou la description
 * contient le mot recherché
 */
    session_start();
    if (!isset($_SESSION['login']))
    {
            header('Location: index.php?erreurDroits=1');
            exit();
    }
    else if(!isset($_SESSION['agendaVu']))
    {
            $_SESSION['agendaVu'] = $_SESSION['id'];
    }
    if (isset($_POST["recherche"]) && $_POST["recherche"]!="")
    {
            include 'connecteur.php';
?>
<!DOCTYPE html>
<html>
            <?php 
            $recherche = "%".$_POST["recherche"]."%";
            $eventsTrouves = SearchEvents($_SESSION['agendaVu'],$recherche);
            if ($eventsTrouves)
            {?>

                <head>

                    <title>Recherche d'événement dans l'agenda de <?php echo $eventsTrouves[0]["uti_pre"]." ".$eventsTrouves[0]["uti_nom"]?></title>
            </head>
            <body>
            <h1>Résultats trouvés pour le(s) terme(s) "<?php echo $_POST["recherche"];?>" dans l'agenda de <?php echo $eventsTrouves[0]["uti_pre"]." ".$eventsTrouves[0]["uti_nom"]?></h1>
                    <?php
                    $i = count($eventsTrouves);
                    foreach($eventsTrouves as $event)
                    {
                            echo "<p>";
                            $dateDebut = new DateTime($event["eve_debut"]);
                            $dateFin   = new DateTime($event["eve_fin"]);
                            $jourDebut = $dateDebut->format("d/m/Y à H:i");
                            $jourFin   = $dateFin->format("d/m/Y à H:i");
                            echo $i." - ".$event["eve_lib"]." - du $jourDebut au $jourFin<br />".$event["eve_desc"]."</p>";
                            $i++;
                    } 
            }
            else
            {
                    echo "Aucun événement correspondant.";
            }?>
            <p><a href="index.php">Retour à l'agenda</a></p><?php
    }
    else
    {
        header ('Location: index.php');
    }
     ?>
</body>