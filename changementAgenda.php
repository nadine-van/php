<?php
/**
 * changementAgenda.php - Page de traitement. Affiche l'agenda de la personne 
 * séléctionnée dans le menu déroulant.
 */

session_start();
/* si l'utilisateur n'est pas logué : retour à index.php */
if (!isset($_SESSION['agendaVu']) || !isset($_SESSION['vue']))
{
        header('Location: index.php?erreurDroits=1');
        exit();
}
/* si l'utilisateur connecté a séléctionné un agenda dans la liste, l'agenda
 * affiché sera celui-ci. */
else if (isset($_POST['agendaVu']))
{
        $_SESSION['agendaVu'] = $_POST['agendaVu'];
}
/* Que l'utilisateur ait séléctionné un agenda ou ait tenté d'accéder directement
 * à la page, il est finalement redirigé */
if ($_SESSION['vue'] == 1)
{
        header('Location: jour.php');
        exit();
}
else if ($_SESSION['vue'] == 2)
{
        header('Location: semaine.php');
        exit();
}
else
{
        header('Location: mois.php');
        exit();
}
?>