<?php
/**
 * export.php - Page de traitement. Génère le fichier d'export, le dump, ou 
 * lance l'importation.
 */
session_start();
/* Si l'utilisateur n'est pas connecté, retour en page d'accueil */
if (!isset($_SESSION["id"]))
{
        header('Location: index.php?erreurDroits=1');
        exit();
}
/* Les conditions pour accéder à cette page :
 * soit c'est un utilisateur connecté qui a cliqué sur le bouton Exporter
 * soit c'est un administrateur qui a cliqué sur Générer un dump
 * soit c'est un utilisateur qui a uploadé une sauvegarde */
else if (isset($_POST['exportID']) || ($_SESSION["admin"] == 1 && isset($_POST['dump'])) || (isset($_FILES['upload']) && $_FILES['upload']['error'] == 0))
{
        include('connecteur.php');
        /* Si 'exportID' est valorisé, c'est que l'utilisateur a cliqué sur Exporter */
        if (isset($_POST['exportID']))
        {
                $id         = intval($_POST['exportID']);
                $evenements = SelectEveryEvent($id);
                if (sizeof($evenements) > 0)
                {
                        $nomfichier = EcritureScript($evenements, $id);
                        Telechargement($nomfichier);
                        exit();
                }
        }
        /* Si 'dump' est valorisé c'est que l'utilisateur est un admin qui a cliqué sur Générer un dump */
        else if ($_SESSION["admin"] == 1 && isset($_POST['dump']))
        {
                $nomfichier = GenereDump();
                Telechargement($nomfichier);
        }
        /* Si 'upload' est valorisé c'est que l'utilisateur a uploadé une sauvegarde. */
        else if (isset($_FILES['upload']))
        {
                /* Si l'extension du fichier n'est pas wgd, message d'erreur */
                if (substr($_FILES['upload']['name'], -4, 4) != ".wgd")
                {
                        header('Location : options.php?error=1');
                }
                else
                {
                        /* cree un répertoire sauvegardes/n° utilisateur s'il n'existe pas */
                        $path = "sauvegardes/" . $_SESSION['id'] . "/";
                        if (!is_dir($path))
                        {
                            mkdir($path, 0777, true);
                        }
                        move_uploaded_file($_FILES['upload']['tmp_name'], $path . "save.txt");
                        $myfile  = fopen($path . "save.txt", 'r');
                        $requete = "";
                        $i       = 1; //compteur pour la boucle while qui suit
                        while (($line = fgets($myfile)) !== false)
                        {
                                $requete = $requete . $line;
                                $i++;
                        }
                        $reussi = ExecuteRequete($requete);
                        if ($reussi)
                        {
                                header('Location: options.php?error=0');
                        }
                        else
                        {
                                header('Location: options.php?error=2');
                        }
                }
        }
}
/* Dans les autres cas, retour à la vue précédente */
else{
        header('Location: index.php');
        exit();
}

/**
 * Génère un fichier texte "sauvegarde-du-jour" avec l'extension .wgd qui 
 * contient des requêtes SQL permettant de recréer les événements d'un utilisateur
 * @param array $evenements
 * @param int $id
 * @return string
 */
function EcritureScript($evenements, $id)
{
        $maintenant = Maintenant();
        $nomfichier = "sauvegarde-du-" . $maintenant . ".wgd";
        $myfile     = fopen($nomfichier, "w");
        $chaine1    = "DELETE from EVENEMENT where eve_uti=:id ; INSERT into EVENEMENT (eve_lib, eve_desc, eve_debut, eve_fin, eve_eta, eve_uti) values ('" . $evenements[0]["eve_lib"] . "','" . $evenements[0]["eve_desc"] . "','" . $evenements[0]["eve_debut"] . "','" . $evenements[0]["eve_fin"] . "'," . $evenements[0]["eve_eta"] . ",:id),";
        for ($i = 1; $i < sizeof($evenements); $i++)
        {
                $chaine2 = $chaine1 . "('" . $evenements[$i]["eve_lib"] . "','" . $evenements[$i]["eve_desc"] . "','" . $evenements[$i]["eve_debut"] . "','" . $evenements[$i]["eve_fin"] . "'," . $evenements[$i]["eve_eta"] . ",:id )";
        } //spécifier :id au lieu de $evenements[$i]["eve_uti"] permet à n'importe quel utilisateur d'installer cette sauvegarde.
        $chaine1 = $chaine2 . ";";
        $chaine2 = $chaine1;
        $chaine  = Aes128_cbc_encrypt($chaine1);
        fwrite($myfile, $chaine);
        return $nomfichier;
}

/**
 * Fonction qui va crypter la requete avant de la sauvegarder dans un fichier .wgd
 * @param string $data
 * @param string $key
 * @param string $iv
 * @return string
 */
function Aes128_cbc_encrypt($data, $key = "sdqsd", $iv = "fREGQ")
{
        if (16 !== strlen($key))
        {
                $key = hash('MD5', $key, true);
        }

        if (16 !== strlen($iv))
        {
                $iv = hash('MD5', $iv, true);
        }
        $padding = 16 - (strlen($data) % 16);
        $data .= str_repeat(chr($padding), $padding);
        return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv);
}

/**
 * Fonction qui va decrypter la requete contenue dans le fichier avant de l'executer
 * @param string $data
 * @param string $key
 * @param string $iv
 * @return string
 */
function Aes128_cbc_decrypt($data, $key = "sdqsd", $iv = "fREGQ")
{
        if (16 !== strlen($key))
        {
                $key = hash('MD5', $key, true);
        }
        if (16 !== strlen($iv))
        {
                $iv = hash('MD5', $iv, true);
        }
        $data    = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv);
        $padding = ord($data[strlen($data) - 1]);
        return substr($data, 0, -$padding);
}

/**
 * Lance le téléchargement du fichier
 * @param string $nomfichier
 */
function Telechargement($nomfichier)
{
        $extension = substr($nomfichier, strpos($nomfichier, '.') + 1);
        header('Content-disposition: attachment; filename=' . $nomfichier);
        header('Content-type: application/' . $extension);
        readfile($nomfichier);
}

/**
 * Retourne la date du jour sous forme de chaîne de caractère
 * @return string
 */
function Maintenant()
{
        $maintenant = new DateTime();
        $maintenant = $maintenant->format('Y-m-d-H\hi');
        return $maintenant;
}
?>