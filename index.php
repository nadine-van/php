<?php
/**
 * index.php - Formulaire de connexion à renseigner pour accéder à l'application
 */
?>
<!DOCTYPE html>
<html>
<head>
	<title>Connexion - Calendrier M2L</title>
		<meta charset="utf-8">
		<link href="css/style.css" rel='stylesheet' type='text/css' />
		<!--Webfonts-->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,300,600,700' rel='stylesheet' type='text/css'>
		<!--Webfonts-->
</head>
    <?php
    /* Variables globales pour savoir où rediriger l'utilisateur */
    $page               = array(
                    1 => "jour",
                    2 => "semaine",
                    3 => "mois"
    );
    /* Si une session est déja en cours : l'utilisateur est redirigé vers la 
     * vue préférée.
     */
    if(isset($_SESSION['login']) && isset($_SESSION['vue']))
    {
                    header('Location: ' . $page[$_SESSION['vue']] . '.php');
    }
    /* Si l'utilisateur a coché "Se souvenir de moi" lors d'une précédente session,
     * ses informations de session ont été stockées dans des cookies.
     * Une session est ouverte automatiquement et l'utilisateur est redirigé vers 
     * sa vue préférée.
     */    
    else if (isset($_COOKIE['cooklogin']) && isset ($_COOKIE['cookid']) && isset($_COOKIE['cooknom']) && isset ($_COOKIE['cookprenom']) && isset($_COOKIE['cookvue']) && isset($_COOKIE['cookadmin']))
    {
                    $_SESSION['login']    = $_COOKIE['cooklogin'];
                    $_SESSION['id']       = $_COOKIE['cookid'];
                    $_SESSION['nom']      = $_COOKIE['cooknom'];
                    $_SESSION['prenom']   = $_COOKIE['cookprenom'];
                    $_SESSION['vue']      = $_COOKIE['cookvue'];
                    $_SESSION['admin']    = $_COOKIE['cookadmin'];
                    $_SESSION['agendaVu'] = $_COOKIE['cookid'];
                    header('Location: ' . $page[$_SESSION['vue']] . '.php');
    }
    /* Si l'utilisateur n'est pas connecté et n'a pas coché 'Se souvenir de moi'
     * , affichage du formulaire pour une nouvelle connexion.
     */
    else
    {
?>
<body>
	<div class="main">
		<div class="login-form">
			<h1>Connexion de l'utilisateur</h1>
            <?php if (isset($_GET["erreurLogin"])) 
                  { ?>
            <div class="erreur">Votre mot de passe et/ou votre authentifiant est incorrect. Veuillez réessayer.</div>
            <?php }
                  else if (isset($_GET["erreurDroits"])) 
                  { ?>
            <div class="erreur">Vous devez vous connecter afin d'avoir accès à la page désirée.</div>
           	<?php 
                  } ?>
			<form method="POST" action="login.php">
				<input type="text" name="login" Placeholder="Nom d'utilisateur" autofocus required>
				<input type="password" name="mdp" Placeholder="Mot de passe" required>
				<div class="submit">
					<input type="submit" value="SE CONNECTER" >
				</div>
				<label class="remember_me"><input type="checkbox" name="remember" id="remember" value="coche">Se souvenir de moi</label>
			</form>
		</div>
        </div>		 		
</body>
<?php } ?>
</html>