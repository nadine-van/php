<?php 
/**
 * resume.php - Affiche le détail d'un événément et les options permettant de
 * le modifier et de le supprimer. 
 */
session_start(); 
if (!isset($_SESSION['login']))
{
        header('Location: index.php?erreurDroits=1');
        exit();
}
else if (isset($_GET['idEvent']))
{
    
            include ('connecteur.php');
            $evenement = SelectEvent($_GET['idEvent']);
            if(sizeof($evenement)>0){
                $evenement = $evenement[0];

?>
<!DOCTYPE html>
<html>
    <head>
	<title>Agenda de <?php echo $evenement['uti_nom']." ".$evenement['uti_pre'] ;?> - Calendrier M2L</title>
	<meta charset="utf-8">
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,300,600,700' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <h1><?php echo $evenement['eve_lib'] ;?></h1>
        
        <?php   $jour         = substr($evenement['eve_debut'], 8, 2);
                $mois         = substr($evenement['eve_debut'], 5, 2);
                $annee        = substr($evenement['eve_debut'], 0, 4);
                $heureDebut   = substr($evenement['eve_debut'], 11, 2);
                $minutesDebut = substr($evenement['eve_debut'], 14, 2);
                $dateDebutSTR = substr($evenement['eve_debut'], 0, 10);
                $dateFinSTR   = substr($evenement['eve_fin'], 0, 10);

                $timestamp   = mktime(0, 0, 0, $mois, $jour, $annee);
                $listeJours  = array(
                    1 => 'Lundi',
                    2 => 'Mardi',
                    3 => 'Mercredi',
                    4 => 'Jeudi',
                    5 => 'Vendredi',
                    6 => 'Samedi',
                    7 => 'Dimanche'
                );
                $listeMois   = array(
                    1 => "Janvier",
                    2 => "Février",
                    3 => "Mars",
                    4 => "Avril",
                    5 => "Mai",
                    6 => "Juin",
                    7 => "Juillet",
                    8 => "Août",
                    9 => "Septembre",
                    10 => "Octobre",
                    11 => "Novembre",
                    12 => "Décembre"
                );
                $jourSemaine = date("N", $timestamp);
                $moisSTR     = date("n", $timestamp);
                $dateDebut   = $listeJours[$jourSemaine] . " " . $jour . " " . $listeMois[$moisSTR] . " " . $annee;

                $jour       = substr($evenement['eve_fin'], 8, 2);
                $mois       = substr($evenement['eve_fin'], 5, 2);
                $annee      = substr($evenement['eve_fin'], 0, 4);
                $heureFin   = substr($evenement['eve_fin'], 11, 2);
                $minutesFin = substr($evenement['eve_fin'], 14, 2);

                $timestamp   = mktime(0, 0, 0, $mois, $jour, $annee);
                $jourSemaine = date("N", $timestamp);
                $moisSTR     = date("n", $timestamp);
                $dateFin     = $listeJours[$jourSemaine] . " " . $jour . " " . $listeMois[$moisSTR] . " " . $annee;

                ?>
        
        <p>
            <?php
                echo "Le ".$dateDebut.", de ".$heureDebut."h".$minutesDebut." à ".$heureFin."h".$minutesFin;
            ?>
        </p>
        <p>
            <?php
                echo $evenement['eve_desc'];
            ?>
        </p>
        
        <p>Statut : 
            <?php
            if ($evenement['eve_eta'] == 1)
            {
                    echo "Occupé(e)";
            }
            else
            {
                    echo "Disponible";
            }
            ?>
        </p>
        <?php 
        //si l'événement est celui de l'utilisateur connecté, il a le droit de le modifier ou de le supprimer
        if($evenement['uti_id'] == $_SESSION['id'])
        { ?>
        <p>                    
                <form class="formulaire" method='POST' action="evenement.php">
                        <input type="hidden" name="actiontype" id="actiontype" value="modifier" />
                        <input type="hidden" name="idEvent" id="idEvent" value="<?php echo $_GET['idEvent']; ?>" />
                        <input type="hidden" name="intitule" id="intitule" value="<?php echo $evenement['eve_lib']; ?>" />
                        <input type="hidden" name="description" id="description" value="<?php echo $evenement['eve_desc']; ?>" />
                        <input type="hidden" name="dateDebut" id="dateDebut" value="<?php echo $dateDebutSTR;?>" />
                        <input type="hidden" name="dateDebutEntiere" id="dateDebutEntiere" value="<?php echo $dateDebut;?>" />
                        <input type="hidden" name="heureDebut" id="heureDebut" value="<?php echo $heureDebut;?>" />
                        <input type="hidden" name="minutesDebut" id="minutesDebut" value="<?php echo $minutesDebut;?>" />
                        <input type="hidden" name="dateFin" id="dateFin" value="<?php echo $dateFinSTR;?>" />
                        <input type="hidden" name="dateFinEntiere" id="dateFinEntiere" value="<?php echo $dateFin;?>" />
                        <input type="hidden" name="heureFin" id="heureFin" value="<?php echo $heureFin;?>" />
                        <input type="hidden" name="minutesFin" id="minutesFin" value="<?php echo $minutesFin;?>" />
                        <input type="hidden" name="etatDispo" id="etatDispo" value="<?php echo $evenement['eve_eta'];?>" />
                        <input type="submit" value="Modifier l'événement" />
                </form>
                <form class="formulaire" method='POST' action="traitement.php">
                        <input type="hidden" name="idEvent" id="idEvent" value="<?php echo $_GET['idEvent'];?>" />
                        <input type="hidden" name="actiontype" id="actiontype" value="delete" />
                        <input type="submit" value="Supprimer l'événement" />
                </form>
        </p>
  <?php }?>
    </body>
</html>
<?php 
    } 
} ?>