<?php
/**
 * login.php - Page de traitement des données transmises via le formulaire de 
 * connexion. Redirige l'utilisateur au bon endroit selon les informations
 * données.
 */
?>
<!DOCTYPE html>
<html>
<head>
	<title>Connexion calendrier M2L</title>
</head>
<body>
<?php
include 'connecteur.php';
/* L'utilisateur a entré un identifiant et un mot de passe : on vérifie que ces 
 * éléments correspondent bien à un utilisateur grâce à la fonction SelectAll()
 * Si c'est le cas on ouvre une session. 
 * Si la case "Se souvenir de moi" a été cochée on sauvegarde les informations 
 * du compte dans des cookies pour la prochaine connexion.
 * Enfin l'utilisateur est redirigé vers la dernière vue consultée (jour, semaine
 * ou mois).
 */
if (isset($_POST['login']) && isset($_POST['mdp'])) 
{
		$requete = SelectAll($_POST['login'], $_POST['mdp']);
		if (count($requete) == 1)
                {
				session_start();
				$_SESSION['login']     = $_POST['login'];
				$_SESSION['id']        = $requete[0]["uti_id"];
				$_SESSION['nom']       = $requete[0]["uti_nom"];
				$_SESSION['prenom']    = $requete[0]["uti_pre"];
				$_SESSION['vue']       = $requete[0]["uti_vue"];
				$_SESSION['admin']     = $requete[0]["uti_admin"];
				$_SESSION['collegues'] = SelectCollegues($requete[0]["uti_id"]);
				$_SESSION['agendaVu']  = $requete[0]["uti_id"];//à la connexion l'agenda affiché est toujours celui de l'utilisateur
				if (isset($_POST['remember'])) {
						setcookie('cooklogin', $_SESSION['login'], time() + 2592000, " /");
						setcookie('cookid', $_SESSION['id'], time() + 2592000, " /");
						setcookie('cooknom', $_SESSION['nom'], time() + 2592000, " /");
						setcookie('cookprenom', $_SESSION['prenom'], time() + 2592000, " /");
						setcookie('cookvue', $_SESSION['vue'], time() + 2592000, " /");
						setcookie('cookadmin', $_SESSION['admin'], time() + 2592000, " /");
				}
				if ($requete[0]["uti_vue"] == 1) 
                                {
						header('Location: jour.php');
						exit();
				}
                                else if ($requete[0]["uti_vue"] == 2) 
                                {
						header('Location: semaine.php');
						exit();
				}
                                else 
                                {
						header('Location: mois.php');
						exit();
				}
		}
                /* Si l'authentification échoue : retour au formulaire avec message
                 * d'erreur "Mot de passe et/ou authentifiant incorrect"
                 */
                else 
                {
				header('Location: index.php?erreurLogin=1');
				exit();
		}
}
/* Si l'accès à la page a été tenté sans transmettre d'identifiant :
 * retour au formulaire avec message d'erreur "Vous devez vous connecter".
 * Les personnes déjà authentifiées qui tenteraient d'accéder directement à cette
 * page se feront ensuite rediriger par index.php vers leur vue préférée */
else 
{
		header('Location: index.php');
		exit();
}
?>	 		
</body>
</html>