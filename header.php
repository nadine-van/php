<?php
/**
 * header.php - entête des vues de l'agenda (jour.php, semaine.php et mois.php)
 */
?>
        <meta charset="utf-8">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.min.css" rel='stylesheet' type='text/css' />
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <!--Webfonts-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,300,600,700' rel='stylesheet' type='text/css'>
        <!--Webfonts-->
</head>
<body <?php
if (isset($pageCourante) && $pageCourante == "semaine")
{
?>id="semainier"<?php
}
?>>
<header>
    <table>
        <tr>
            <td class="left">
                <div id="logoHeader">
                    <img src="img/logo2.gif" alt="Logo M2L">
                </div>
            </td>
            <td class="right">
                <div id="bienvenue">Connecté(e) en tant que : <a href='options.php' ><?php
echo "{$_SESSION['prenom']} {$_SESSION['nom']}"; ?></a></div>
                <div id="deconnecter">
                    <form method="post" action="deconnexion.php">
                        <input type="submit" value="Se déconnecter" />
                    </form>
                </div>
            </td>
        </tr>
    </table>
</header>
