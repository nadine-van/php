<?php
/**
 * deconnexion.php - Page de traitement qui déconnecte l'utilisateur.
 */

    session_start();
    if(isset($_SESSION['vue']) && isset($_SESSION['id']))
    {
            include 'connecteur.php';
            VuePreferee();//sauvegarde dans la BDD la dernière vue
    }
    $_SESSION = array();
    session_destroy();//destruction de la session
    /* destruction des éventuels cookies */
    if (isset($_COOKIE['cooklogin']) || isset ($_COOKIE['cookid']) || isset($_COOKIE['cooknom']) || isset ($_COOKIE['cookprenom']) || isset($_COOKIE['cookvue']) || isset($_COOKIE['cookadmin']))
    {
            setcookie("cookid", '', time() - 2592000, "/");
            setcookie("cooknom", '', time() - 2592000, " /");
            setcookie("cookprenom", '', time() - 2592000, "/");
            setcookie("cooklogin", '', time() - 2592000, " /");
            setcookie("cookvue", '', time() - 2592000, "/");
            setcookie("cookadmin", '', time() - 2592000, " /");
    }
    header('Location: index.php');
?>