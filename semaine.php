<?php
/**
 * semaine.php - Affiche le planning sur une semaine
 */

/* Démarrage session et vérification des droits d'accès.
Redirige l'utilisateur en page d'accueil s'il n'est pas logué */
session_start();
if (!isset($_SESSION['login']) || !isset($_SESSION['id'])) 
{
        header('Location: index.php?erreurDroits=1');
        exit();
}
$_SESSION['vue'] = 2; //2 correspond à la vue en cours (ici semaine)
?>
<!DOCTYPE html>
<html>
<head>
	<title>Agenda M2L - Vue hebdomadaire</title>
        
	<?php
        /*chargement de l'entête de la vue et des fonctions de connexion
         * à la base de données */
        include 'header.php';
        include 'connecteur.php';

        /* signale au footer que la page courante est la vue semaine */
        $pageCourante = 'semaine';

        /*création d'un DateTime initialisé à maintenant, à Paris */
        try 
        {
                date_default_timezone_set('Europe/Paris'); //Définition du fuseau horaire par défaut, tant pis pour les utilisateurs étrangers :'D
        }
        catch (Exception $e) 
        {
                exit($e->getMessage());
        }
        $maintenant = new DateTime();

        /* Si une date est spécifiée en paramètre dans l'URL :
         * récupère cette valeur et crée un DateTime $dateCourante 
         * après vérification du format de cette valeur.
         * preg_match() vérifie que la chaîne respecte bien le format 
         * spécifié en regex (YYYY-MM-DD), la découpe et l'affecte
         * à un tableau (ici $split). Ensuite il teste si cette valeur
         * permet de créer une date grâce à checkdate();
         * Sinon par défaut il affecte la valeur du jour courant */
        if (isset($_GET['datecourante']) && preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $_GET['datecourante'], $split) && checkdate($split[2], $split[3], $split[1])) 
        {
                $dateCourante = new DateTime($_GET['datecourante']);
        } 
        else 
        {
                $dateCourante = $maintenant;
        }
        /* Récupération de toutes les valeurs nécéssaires à l'affichage 
         * du calendrier de la semaine */
        $jourCourant     = $dateCourante->format("N"); //Récupère le numéro du jour dans la semaine (de 1 pour lundi à 7 pour dimanche)
        $moisCourant     = $dateCourante->format("n"); //Récupère le numéro du mois sans les zéros initiaux
        $anneeCourante   = $dateCourante->format("Y"); //Récupère l'année complète
        $semaineCourante = $dateCourante->format("W"); //Récupère le numéro de la semaine
        $heureActuelle   = $maintenant->format("H");

        $dateCourante2      = new DateTime($dateCourante->format("Y-m-d")); //Création d'une $dateCourante2 égale à $dateCourante dans le but pouvoir le modifier en gardant $dateCourante inchangé
        $dateDebutSemaine   = $dateCourante2->modify("Monday this week"); //$dateCourante2 devient (ou reste) le lundi de la semaine courante    
        $numeroDebutSemaine = $dateDebutSemaine->format("d");
        $stringDebutSemaine = $dateDebutSemaine->format("Y-m-d");
        $dateFinSemaine     = $dateCourante2->modify("Sunday this week"); //$dateCourante2 devient le dimanche de la semaine courante
        $numeroFinSemaine   = $dateFinSemaine->format("d");
        $moisFinSemaine     = $dateFinSemaine->format("n");
        $anneeFinSemaine    = $dateFinSemaine->format("Y");

        $jour  = array(
            '',
            'Lun.',
            'Mar.',
            'Mer.',
            'Jeu.',
            'Ven.',
            'Sam.',
            'Dim.'
        );
        $mois  = array(
            '',
            'janvier',
            'février',
            'mars',
            'avril',
            'mai',
            'juin',
            'juillet',
            'août',
            'septembre',
            'octobre',
            'novembre',
            'décembre'
        );
        $heure = array();
        for ($i = 0; $i < 24; $i++) {
            if ($i < 10) {
                $heure[] = '0' . $i . ':00';
            } else {
                $heure[] = $i . ':00';
            }
        }

        /* $lundiSuivant est utilisé pour construire $stringFinSemaine, 
         * correspondant au lundi d'après avec une heure de 00:00:00
         * Cela permet de récupérer les évènements du dernier jour de la 
         * semaine. */
        $lundiSuivant               = new DateTime($dateCourante2->modify('+1 day')->format("Y-m-d"));
        $lundiDernier               = new DateTime($dateCourante2->modify('-2 weeks')->format("Y-m-d"));
        $stringLundiDernier         = $lundiDernier->format("Y-m-d");
        $stringFinSemaine           = $lundiSuivant->format("Y-m-d");
        $dateCourante2              = new DateTime($dateCourante2->modify('+1 week')->format("Y-m-d")); //remet la dateCourante2 au lundi courant pour la suite
        $listeEvenementsDeLaSemaine = SelectEvents($_SESSION['agendaVu'], $stringDebutSemaine, $stringFinSemaine);
        $intervalle                 = new DateInterval('P1D');
        if (count($listeEvenementsDeLaSemaine) > 0) 
        {
                /* Le tableau $ecriture[] servira à stocker l'état d'écriture d'un évènement.
                 * Lors de l'affichage d'un événement on affiche l'intitulé sur la première
                 * ligne puis l'heure de début et l'heure de fin sur la seconde (s'il y en
                 * a une). On initialise donc le contenu à 'intitulé' puisque c'est celui 
                 * qui s'affiche en premier. Cette valeur sera amenée à changer en fonction
                 * de la durée de l'événement pour afficher l'événement sur toutes les
                 * cases concernées.
                 */
                for ($i = 0; $i < count($listeEvenementsDeLaSemaine); $i++) {
                    $ecriture[$i]        = 'intitule';
                    $nbEventsParCase[$i] = 1;
                    $listeInchangee[$i]  = $listeEvenementsDeLaSemaine[$i];
                }
        }
        ?>
            <div id="wrapper">
                <div id="sousHeader">
                        <table class="sousHeader">
                            <tr>
                                <td class="left">
                                    <a href="semaine.php?datecourante=<?php
        echo $stringLundiDernier;
        ?>"><div id="boutonPrecedent" class="bouton"><<</div></a>
                                    <a href="semaine.php?datecourante=<?php
        echo $stringFinSemaine;
        ?>"><div id="boutonSuivant" class="bouton">>></div></a>
                                </td>
                                <td id="titreSemaine">
                                    <div><?php
        /* Affichage le numéro de semaine, la date de début et la date de fin */
        echo "[$semaineCourante] Semaine du $numeroDebutSemaine ";
        /* Cas classique : le premier jour de la semaine et le dernier jour
        sont sur le même mois (ex : du 1er au 7 mai 2015) */
        if ($moisFinSemaine == $moisCourant) {
            echo "au $numeroFinSemaine $mois[$moisCourant] $anneeCourante";
        }
        /* Cas alternatif : la semaine chevauche deux années (ex : du 28 
         * décembre 2015 au 03 janvier 2016) */
        else if ($anneeCourante != $anneeFinSemaine) {
            echo "$mois[$moisCourant] $anneeCourante au $numeroFinSemaine $mois[$moisFinSemaine] $anneeFinSemaine";
        }
        /* Dernier cas possible : la semaine chevauche deux mois différents */
        else {
            echo $mois[$moisCourant] . " au $numeroFinSemaine $mois[$moisFinSemaine] $anneeCourante";
        }
        ?>
                        </div>        
                                </td>
                                <td class="right">
                                    <div id="vue">Vue :</div>
                                    <div id="boutonJour" class="bouton"><a href="jour.php?datecourante=<?php
        echo $stringDebutSemaine;
        ?>">Jour</a></div>
                                    <div id="boutonSemaine" class="bouton">Semaine</div>
                                    <div id="boutonMois" class="bouton"><a href="mois.php?moiscourant=<?php
        echo $moisCourant;
        ?>&anneecourante=<?php
        echo $anneeCourante;
        ?>">Mois</a></div>
                                </td>
                            </tr>
                        </table>
                </div>
        <table id="enteteAgendaSemaine">
                                <!-- Construction des entêtes de la table contenant les 
                                jours de la semaine ainsi que leurs dates. -->
                                <tr>
                                        <th class="colonne0"></th>
                                        <?php
        for ($i = 1; $i < 8; $i++) 
        {
        ?>
                                        <th class="<?php
            echo "colonne" . $i;
        ?>"><?php
            if ($dateCourante2->format('m') == $moisCourant) 
            {
                    echo "$jour[$i] {$dateCourante2->format('d')}/$moisCourant";
            }
            else
            {
                    echo "$jour[$i] {$dateCourante2->format('d')}/$moisFinSemaine";
            }
        ?></th>
                                        <?php
            //Création d'un tableau contenant les numéros des jours de la semaine courante
            //Ce tableau sera utilisé dans la condition de l'affichage des évènements
            $numeroJourSemaine[$i] = $dateCourante2->format('d');
            $dateCourante2->add($intervalle);
        }
        ?>

                                </tr>
        </table>
                <div id="agendaSemaine">
                        <table id="tableAgendaSemaine" border="1">
                        <?php
        /* Construction des 24 <tr> permettant la division horaire d'une journée
         * La colonne 0 spécifie l'heure correspondant à la ligne. Ces cases 
         * ont un id "trRowX", X correspondant à l'heure (par exemple "thRow23"
         * pour la case contenant 23:00). 
         * rowspan permet de spécifier sur combien de lignes porte la case horaire,
         * ici 2, ce qui permet de fractionner l'heure en deux demi-heure et donc 
         * d'avoir des plages horaires en xx:00 et en xx:30.
         * Les colonnes 1 à 7 affichent les cases des jours correspondants. */
        for ($i = 0; $i < 24; $i++) 
        {
            /* Deux boucles pour chaque heure : une pour la première demi-heure
            une pour la seconde*/
            for ($e = 0; $e < 2; $e++)
            {
        ?>
                                <tr>      
                                        <?php
                if ($e == 0) 
                {
        ?>
                                        <!-- Affichage de la première case de la ligne (celle contenant l'heure)
                                        seulement sur la première ligne
                                        Si l'on affiche la semaine actuelle, on affecte à la case représentant 
                                        l'heure courante l'id "current". La fonction javascript current () déclarée
                                        avant </body> se chargera d'afficher l'agenda au niveau de cette heure -->
                                        <th class="colonne0" id="<?php
                    if ($i == $heureActuelle && $maintenant->format("W") == $semaineCourante)
                    {
                        echo "current";
                    }
                    else
                    {
                        echo "thRow" . $i;
                    }
        ?>" rowspan="2"><?php
                    echo "$heure[$i]";
        ?></th>
                                        <?php
                } //fin de la condition d'affichage du <th>

                /* Affichage des <td> de la row correspondant à une heure "pile" type 8:00 */
                for ($j = 1; $j < 8; $j++)
                {
                    //passe au mois suivant quand on atteint le numéro 1
                    if ($numeroJourSemaine[$j] == 1) 
                    {
                            $moisCourant = $moisFinSemaine;
                    }
        ?>
                                        <!-- Affichage des cellules d'une heure donnée pour chaque jour de la semaine.
                                        Crée pour chaque ligne 7 cellules identifiées par leur rang (ligne et colonne)
                                        (et donc par leur jour et par leur heure puisque colonne 1 ligne 3 = lundi 3h00)
                                        Au clic, appelle un formulaire de création d'événement et lui transmet le jour,
                                        la date et l'heure de la cellule.
                                        Vérifie s'il y a des événements à afficher cette semaine, et si c'est le cas,
                                        regarde dans cette liste si l'heure de début d'un événement correspond au jour
                                        et à l'heure de la cellule en cours de création, et enfin si c'est le cas crée 
                                        dans la cellule une div où s'affiche l'intitulé ou l'heure de l'événement -->
                                        <td class="<?php
                    echo "colonne" . $j;
        ?>" 
                                            id="r<?php
                    echo $i;
        ?>c<?php
                    echo $j;
        ?>" 
                                            onclick="window.open('evenement.php?date=<?php
                    if (strlen($moisCourant) == 1) 
                    {
                            $moisCourant = '0' . $moisCourant;
                    }
                    if (strlen($numeroJourSemaine[$j]) == 1) 
                    {
                            $numeroJourSemaine[$j] = '0' . $numeroJourSemaine[$j];
                    }
                    echo $anneeCourante . '-' . $moisCourant . '-' . $numeroJourSemaine[$j];
        ?>&heure=<?php
                    if (strlen($i) == 2) 
                    {
                            echo $i;
                    }
                    else 
                    {
                            echo "0" . $i;
                    }
        ?>&minutes=<?php
                    if ($e == 0) 
                    {
                            echo '00';
                    }
                    else 
                    {
                            echo '30';
                    }
        ?>', 'Ajout Evénement', 'height=450, width=400, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;">
                                                <?php
                    //vérifie l'existence d'événements pour la semaine en cours
                    if (count($listeEvenementsDeLaSemaine) > 0) 
                    {
                        /* détermine la durée de l'événement et l'heure à afficher dans la cellule */

                            for ($c = 0; $c < count($listeEvenementsDeLaSemaine); $c++) 
                            {
                                    $heureDebut    = substr($listeInchangee[$c]['eve_debut'], 11, 5);
                                    $heureFin      = substr($listeInchangee[$c]['eve_fin'], 11, 5);
                                    $heureAffichee = $heureDebut . " - " . $heureFin;

                                    /*Si le jour et l'heure de la cellule correspondent bien à ceux de l'événement,
                                     * On affiche l'événement. Ici trois cas de figure sont possibles :
                                     * - l'événement commence à la demie (xx:30) et dure au moins une demie-heure
                                     * - l'événement commence à l'heure pile (xx:00) et dure au moins une heure
                                     * - l'événement dure une demie-heure et finit à la demie. 
                                     * */
                                    if (substr($listeEvenementsDeLaSemaine[$c]['eve_debut'], 8, 2) == $numeroJourSemaine[$j] && substr($listeEvenementsDeLaSemaine[$c]["eve_debut"], 11, 2) == substr($heure[$i], 0, 2)) 
                                    {
                                            //On fait les différences entre les heures de datefin/datedebut ainsi que celle entre les minutes
                                            $differenceHeures  = substr($listeEvenementsDeLaSemaine[$c]['eve_fin'], 11, 2) - substr($listeEvenementsDeLaSemaine[$c]['eve_debut'], 11, 2);
                                            $differenceMinutes = substr($listeEvenementsDeLaSemaine[$c]['eve_fin'], 14, 2) - substr($listeEvenementsDeLaSemaine[$c]['eve_debut'], 14, 2);
                                            $minutesDebut      = substr($listeInchangee[$c]['eve_debut'], 14, 2);
                                            $minutesFin        = substr($listeInchangee[$c]['eve_fin'], 14, 2);

                                            if ($e == 1) 
                                            {
                                                    $nouvelleDateDebut = substr($listeEvenementsDeLaSemaine[$c]['eve_debut'], 11, 2) + 1;
                                                    //Ajout d'un 0 ou non en fonction de si $nouvelleDateDebut est composé d'un ou de deux chiffres
                                                    if ($nouvelleDateDebut < 10) 
                                                    {
                                                            $listeEvenementsDeLaSemaine[$c]['eve_debut'] = substr($listeEvenementsDeLaSemaine[$c]['eve_debut'], 0, 10) . " 0" . $nouvelleDateDebut . substr($listeEvenementsDeLaSemaine[$c]['eve_debut'], 13, 6);
                                                    }
                                                    else 
                                                    {
                                                            $listeEvenementsDeLaSemaine[$c]['eve_debut'] = substr($listeEvenementsDeLaSemaine[$c]['eve_debut'], 0, 10) . " " . $nouvelleDateDebut . substr($listeEvenementsDeLaSemaine[$c]['eve_debut'], 13, 6);
                                                    }
                                            }

                                            /* Premier cas : l'événement dure au moins une demi-heure et commence à la demie (xx:30) :
                                             * La cellule n'affiche rien et $ecriture reste valorisée à 'intitule' pour indiquer à la
                                             * cellule d'en dessous que c'est à elle d'afficher l'intitule */
                                            if ($minutesDebut == 30 && $ecriture[$c] == 'intitule' && $e == 0) 
                                            {

                                            }
                                            /* Deuxième cas : l'événement dure une demi-heure et finit à la demie,
                                             * ou l'événement dure plus d'une heure.
                                             * En fonction de ce que lui indique $ecriture elle affiche soit l'intitulé,
                                             * soit l'heure, soit une case vide (dans tous les cas colorée).
                                             * Ensuite $ecriture change de valeur pour indiquer à la case d'en dessous
                                             * d'afficher :
                                             * - l'heure, si la case en cours a affiché l'intitulé
                                             * - une case colorée vide si la case est cours a affiché l'heure
                                             */
                                            else if ($differenceHeures > 0 || ($differenceHeures == 0 && $minutesFin == 30) || $minutesFin == 59) 
                                            {
                                                    if ($ecriture[$c] == 'intitule') 
                                                    {
                            ?>
                                                                                                            <div class="evenement" onclick="window.open('resume.php?idEvent=<?php
                                                            echo $listeEvenementsDeLaSemaine[$c]['eve_id'];
                            ?>', 'Vue Evénement', 'height=450, width=400, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no')">
                                                                                                                <?php
                                                            echo $listeEvenementsDeLaSemaine[$c]['eve_lib'];
                            ?>
                                                                                                            </div>								<?php
                                                            if ($minutesFin == 59) 
                                                            {
                                                                    $ecriture[$c] = 'vide';
                                                            }
                                                            else 
                                                            {
                                                                    $ecriture[$c] = 'heure';
                                                            }
                                                    }
                                                    else if ($ecriture[$c] == 'heure') 
                                                    {
                        ?>
                                                                                                        <div class="evenement">
                                                                                                                <?php
                                                            echo $heureAffichee;
                        ?>
                                                                                                        </div>
                                                                                        <?php
                                                            $ecriture[$c] = 'vide';
                                                    }
                                                    else if ($ecriture[$c] == 'vide') 
                                                    {   ?>
                                                                                                                <div class="evenement noBorderTop">&nbsp;</div>
                                       <?php        }
                                                    /* si l'événement finit dans la première demi-heure, on change son
                                                     * heure de fin pour que l'événement ne s'affiche pas dans la
                                                     * cellule suivante.*/
                                                    if ($differenceHeures == 0 && $minutesFin == 30) 
                                                    {
                                                            $listeEvenementsDeLaSemaine[$c]['eve_fin'] = 0;
                                                            $ecriture[$c]                              = '';
                                                    }
                                            }
                                    } //fin de la condition "Si l'événement correspond bien à la date et l'heure de cellule"	
                            } //fin de la boucle sur cheque événement de la liste
                    } //fin de la condition "S'il existe au moins un événement pour cette semaine"
        ?>

                                        </td>
                                        <?php
                } //fermeture de la cellule et passage à la cellule suivante
        ?>
                                </tr>
                        <?php
            } //fin de la construction de la ligne <tr>
        } //boucle deux fois pour construire deux lignes <tr> 
        ?>
                        </table>
                </div>
        <?php
        include('footer.php');
        ?>