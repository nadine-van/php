<?php 
/**
 * evenement.php - Formulaire de création ou de modification d'un événement
 */
?>
<!DOCTYPE html>
<html>
    <head>
	<title>Evénement - Calendrier M2L</title>
	<meta charset="utf-8">
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,300,600,700' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <h1>        
        <?php
/* Affiche le titre en fonction de s'il s'agit d'une demande de modification 
 * d'un événement existant (l'utilisateur a cliqué sur "Modifier l'événement")
 * ou s'il s'agit d'une création d'événement (l'utilisateur a cliqué sur une 
 * case de son agenda */
if ((isset($_POST['actiontype']) && $_POST['actiontype'] == "modifier") || (isset($_GET['actiontype']) && $_GET['actiontype'] == "modifier")) 
{
        echo "Modifier l'";
}
else 
{
        echo "Nouvel ";
}
?>événement
        </h1>
        <form method="POST" action="traitement.php">
            <p>
                <input type="text" name="intitule" placeholder="Intitulé de l'événement" 
<?php
/* En cas d'erreur de traitement ou de modification les valeurs du formulaire 
 * sont retransmises pour recommencer la saisie */
if (isset($_GET["intitule"])) 
{
        echo "value=\'" . $_GET["intitule"];
}
else if (isset($_POST["intitule"])) 
{
        echo "value='" . $_POST["intitule"];
}
?>'  autofocus required />
            </p>
            <p>
                <?php
if (isset($_GET["date"]) || isset($_POST['actiontype'])) 
{
        if (isset($_GET["date"])) 
        {
                $jour  = substr($_GET["date"], 8, 2);
                $mois  = substr($_GET["date"], 5, 2);
                $annee = substr($_GET["date"], 0, 4);
        }
        else if (isset($_POST['actiontype'])) 
        {
                $jour  = substr($_POST["dateDebut"], 8, 2);
                $mois  = substr($_POST["dateDebut"], 5, 2);
                $annee = substr($_POST["dateDebut"], 0, 4);
        }
        $timestamp   = mktime(0, 0, 0, $mois, $jour, $annee);
        $listeJours  = array(
                1 => 'Lundi',
                2 => 'Mardi',
                3 => 'Mercredi',
                4 => 'Jeudi',
                5 => 'Vendredi',
                6 => 'Samedi',
                7 => 'Dimanche'
        );
        $listeMois   = array(
                1 => "Janvier",
                2 => "Février",
                3 => "Mars",
                4 => "Avril",
                5 => "Mai",
                6 => "Juin",
                7 => "Juillet",
                8 => "Août",
                9 => "Septembre",
                10 => "Octobre",
                11 => "Novembre",
                12 => "Décembre"
        );
        $jourSemaine = date("N", $timestamp);
        $moisSTR     = date("n", $timestamp);
?>
                
                <label for="dateDebut">Debut</label>  
                <select name="dateDebut" id="dateDebut">
                    <option value="<?php echo $annee . "-" . $mois . "-" . $jour; ?>">
                        <?php echo $listeJours[$jourSemaine] . " " . $jour . " " . $listeMois[$moisSTR] . " " . $annee; ?>
                    </option>
                    <?php } ?>
                </select>
                <select name="heureDebut" id="heureDebut">
<?php
for ($i = 0; $i < 24; $i++) 
{
?>
                   <option value="<?php
        if (strlen($i) == 1) 
        {
                echo "0" . $i;
        } 
        else 
        {
                echo $i;
        }
?>" <?php
        if ((isset($_GET["heure"]) && $_GET["heure"] == $i) || (isset($_POST["heureDebut"]) && $_POST["heureDebut"] == $i)) 
        {
                echo "selected";
        }
?>>
<?php   echo $i; ?>                       
                   </option>
                    <?php
}
?>
                </select>                
                <select name="minutesDebut" id="minutesDebut">
                    <?php
for ($i = 0; $i < 60; $i = $i + 30) {
?>
                   <option value="<?php
        if (strlen($i) == 1) 
        {
                echo "0" . $i;
        } 
        else 
        {
                echo $i;
        }
?>" <?php
        if (isset($_GET["minutes"]) && $_GET["minutes"] == $i) 
        {
                echo "selected";
        }
?><?php
        if (isset($_POST["minutesDebut"]) && $_POST["minutesDebut"] == $i) 
        {
                echo "selected";
        }
?>>
                       <?php
    if (strlen($i) == 1)
    {
            echo "0" . $i;
    }
    else
    {
            echo $i;
    }
?>
                   </option>
                    <?php
}
?>
                </select>
            </p>  
                                    <?php
if (isset($_GET["erreurDate"]) && $_GET["erreurDate"] == 1) 
{
?>
            <p class="erreur">Incohérence au niveau de l'heure de fin.<br />Veuillez choisir une heure plus tardive que l'heure de début.</p>
                    <?php
}
?>
          
            <p>
                <label for="dateFin">Fin</label>  
                <select name="dateFin" id="dateFin">
                    <?php
if (isset($_GET["date"]) || isset($_POST["dateFin"])) 
{
        for ($i = 0; $i < 3; $i++) 
        {
?>
                   <option value="<?php
        echo $annee . "-" . $mois . "-" . $jour;
?>">
                       <?php
        echo $listeJours[$jourSemaine] . " " . $jour . " " . $listeMois[$moisSTR] . " " . $annee;
?></option>
                    <?php
            $timestamp   = strtotime("+1 day", $timestamp);
            $jourSemaine = date("N", $timestamp);
            $moisSTR     = date("n", $timestamp);
            $annee       = date("Y", $timestamp);
            $jour        = date("d", $timestamp);
            $mois        = date("n", $timestamp);
        }
}
?>
                </select>
                <select name="heureFin" id="heureFin">
                    <?php
for ($i = 0; $i < 24; $i++) 
{
?>
                   <option value="<?php
        if (strlen($i) == 1) 
        {
                echo "0" . $i;
        }
        else 
        {
                echo $i;
        }
?>" <?php
        if (isset($_GET["heure"]) && $_GET["heure"] == $i && $_GET["minutes"] == '00') 
        {
                echo "selected";
        } 
        else if (isset($_POST["heureFin"]) && $_POST["heureFin"] == $i && $_POST["minutesFin"] == '00') 
        {
                echo "selected";
        }
?>>
                       <?php
    echo $i;
?>
                   </option>
                    <?php
}
?>
                </select>
                <select name="minutesFin" id="minutesFin">
                    <?php
for ($i = 0; $i < 60; $i = $i + 30) 
{
?>
                   <option value="<?php
        if (strlen($i) == 1) 
        {
                echo "0" . $i;
        }
        else 
        {
                echo $i;
        }
?>" <?php
        if (isset($_GET["minutes"]) && $_GET["minutes"] != $i) 
        {
                echo "selected";
        }
        else if (isset($_POST["minutesFin"]) && $_POST["minutesFin"] != $i) 
        {
                echo "selected";
        }
?>>
                       <?php
        if (strlen($i) == 1) 
        {
                echo "0" . $i;
        }
        else 
        {
                echo $i;
        }
?>
                   </option>
                    <?php
}
?>
                </select>
            </p>
            <p>
                <label for="description">Description</label>
                <textarea name="description" id="description" rows="10" cols="50"><?php
        if (isset($_GET["description"])) 
        {
                echo $_GET["description"];
        }
        else if (isset($_POST["description"])) 
        {
                echo $_POST["description"];
        }
?></textarea>
            </p>
            <p>
                <label for="etatDispo">Disponibilité</label>
                <select name="etatDispo" id="etatDispo">
                   <option value="1" <?php
if ((isset($_GET["etatDispo"]) && $_GET["etatDispo"] == 1) || (isset($_POST["etatDispo"]) && $_POST["etatDispo"] == 1)) {
        echo "selected";
}
?>>Occupé(e)</option>
                   <option value="2" <?php
if ((isset($_GET["etatDispo"]) && $_GET["etatDispo"] == 2) || (isset($_POST["etatDispo"]) && $_POST["etatDispo"] == 2)) {
        echo "selected";
}
?>>Disponible</option>
                </select>
            </p>
            <p>
                <input type="hidden" name="actiontype" id="actiontype" value="<?php
if (isset($_POST['actiontype'])) 
{
        echo $_POST['actiontype'];
} 
else {
        echo "insert";
}
?>" />
                <?php
if (isset($_POST['idEvent'])) 
{
?>
                <input type="hidden" name="idEvent" id="idEvent" value="<?php echo $_POST['idEvent']; ?>" />
                <?php
}
?>
                <input type="submit" value="Enregistrer" />
            </p>
        </form>
    </body>
</html>